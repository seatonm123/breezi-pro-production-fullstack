import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login/Splash'
import Monitor from '@/components/monitor/Monitor'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/monitor',
      name: 'Monitor',
      component: Monitor
    }
  ]
})
