
const AWS = require('aws-sdk');
const sns = new AWS.SNS();

exports.handler = (event, context, cb) => {
  event.Records.forEach((record) => {
    console.log('Stream record: ', JSON.stringify(record, null, 2));
    if (record.eventName == 'INSERT') {
        console.log({INSERT: record});
    } else {
      console.log({NON_INSERT: record});
    }
  });
}
