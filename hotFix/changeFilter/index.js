
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

async function handler(){

  let bldg = await Dynamo.getItem({
    TableName: 'Buildings-matt',
    Key: {
      id: 'bfc61129-18fd-4b7f-9ee3-bd7d43b9c130'
    }
  }).promise();

  bldg = bldg.Item;

  // let vents = bldg.airVents;
  // vents = vents.reduce((a, i) => {
  //   a.push(i.vent ? i.vent : i);
  //   return a;
  // }, []);

  console.log(bldg);

  // Dynamo.updateItem({
  //   TableName: 'Buildings-matt',
  //   Key: {
  //     id: bldg.id
  //   },
  //   UpdateExpression: 'SET airVents = :vents',
  //   ExpressionAttributeValues: {
  //     ':vents': vents
  //   }
  // }).promise().then(res => {
  //   console.log(res);
  // }).catch(err => {
  //   console.log({ERROR: err});
  // });

}

handler();
