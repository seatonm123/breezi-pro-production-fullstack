
const uuid = require('uuid');

const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-matt';

// deviceId
// hardwareVersion
// id
// userId
// airVentAssignments
  // airVentId
  // fromTimestamp
  // userId

// 1283945390910181
// 0.4.0
// uuid
// b414d53c-2160-4461-ba38-49b29d60ebd8
  // 43667728-92dd-4f6b-94f3-df79a221c1c3
  // now
  // b414d53c-2160-4461-ba38-49b29d60ebd8

// 7438188010352073
// 0.4.0
// uuid
// b414d53c-2160-4461-ba38-49b29d60ebd8
  // 7d10b073-5178-42fb-89a8-c5019ff49b95
  // now
  // b414d53c-2160-4461-ba38-49b29d60ebd8

const handler = () => {
  class Device {
    constructor(did, vId){
      this.deviceId = did;
      this.hardwareVersion = '0.4.0';
      this.userId = 'b414d53c-2160-4461-ba38-49b29d60ebd8';
      this.id = uuid();
      this.airVentAssignments = [{
        airVentId: vId,
        fromTimestamp: Math.floor(Date.now()/1000) - 86400,
        userId: 'b414d53c-2160-4461-ba38-49b29d60ebd8'
      }];
    }
  }
  let dev1 = new Device(1283945390910181, '43667728-92dd-4f6b-94f3-df79a221c1c3');
  let dev2 = new Device(7438188010352073, '7d10b073-5178-42fb-89a8-c5019ff49b95');
  Promise.all([dev1, dev2].map(d => {
    Dynamo.putItem({
      TableName: 'Devices-matt',
      Item: d
    }, (err, data) => {
      err
        ? console.error({ERROR: err})
        : console.log(`${d.deviceId} successfully posted`);
    })
  }));
}

handler();
