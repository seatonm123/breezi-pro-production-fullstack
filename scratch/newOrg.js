
const uuid = require('uuid');
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

async function handler(){
  let org = require('./org');
  org.id = uuid();
  Dynamo.putItem({
    TableName: 'Organizations-matt',
    Item: org
  }, (err, data) => {
    err
      ? console.log({ERROR: err})
      : console.log('SUCCESS!!!!');
  });
}

handler();
