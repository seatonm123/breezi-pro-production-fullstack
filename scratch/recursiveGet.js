
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const devId = 7438188010352073;

// function recursiveScan(did, counter, lastKey){
//   console.log(`Running cycle ${counter}`);
//   console.log(lastKey)
//   // return new Promise((resolve, reject) => {
//     let params = {
//       TableName: 'Devices-matt',
//       Limit: 1
//     }
//     if (lastKey) {
//       params.ExclusiveStartKey = lastKey;
//     }
//     try {
//       Dynamo.scan(params, (err, data) => {
//         if (data.Items[0].deviceId === did) {
//           return new Promise(resolve => resolve(data.Items[0]));
//         } else if (data.LastEvaluatedKey) {
//           counter ++;
//           return recursiveScan(did, counter, data.LastEvaluatedKey);
//         } else {
//           return `Device ${did} not found in DB`;
//         }
//       })
//     } catch(err) {
//       return `Device ${did} not found in DB`;
//     }
//   // })
// }
//
// recursiveScan(devId, 1)
//   .then(device => {
//     console.log({FOUND_DEVICE: device});
//   }, err => {
//     console.log(err);
//   })

function overScan(did){

  return new Promise((resolve, reject) => {
    async function recursiveScan(did, counter, lastKey){
      console.log(`Running cycle ${counter}`);
      let params = {
        TableName: 'Devices-matt',
        Limit: 1
      }
      if (lastKey) {
        params.ExclusiveStartKey = lastKey;
      }

      Dynamo.scan(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          if (data.Items[0].deviceId === did) {
            resolve(data.Items[0]);
          } else if (data.LastEvaluatedKey) {
            counter ++;
            return recursiveScan(did, counter, data.LastEvaluatedKey);
          } else {
            reject(`Device ${did} not found in DB`);
          }
        }
      })
    }
    recursiveScan(did, 1);
  });

}

overScan(devId, 1)
  .then(dev => {
    console.log({FOUND_DEVICE: dev});
  }).catch(err => {
    console.log({ERROR: err});
  });
