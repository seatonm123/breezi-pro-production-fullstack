
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

async function handler(){
  let GWAcct = await Dynamo.getItem({
    TableName: 'Organizations-matt',
    Key: {
      name: 'Gateway Test'
    }
  }).promise();
  GWAcct = GWAcct.Item;
  let bldgs = GWAcct.buildings;
  bldgs = await Promise.all(bldgs.map(async b => {
    let bldg = await Dynamo.getItem({
      TableName: 'Buildings-matt',
      Key: {
        id: b
      }
    }).promise();
    return Promise.resolve(bldg.Item);
  }));
  let devices = bldgs.reduce((a, i) =>{
    i.airVents.map(v => {
      a.push(v.device);
    });
    return a;
  }, []);
  fs.writeFile('./devices.json', JSON.stringify(devices, null, 1), (err, data) => {
    err ? console.log({ERROR: err}) : console.log('SUCCESS');
  })
  // let meas = Promise.all(devices.map(async d => {
  //   let meas = await Dynamo.query({
  //     TableName: 'Measurements-matt',
  //     KeyConditionExpression: 'did = :did',
  //     ExpressionAttributeValues: {
  //       ':did': d
  //     }
  //   }).promise();
  //
  // }));
}

handler();
