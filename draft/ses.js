
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();
const SES = new AWS.SES();

async function handler(){

  let users = await Dynamo.scan({TableName: 'Users-matt'}).promise();
  users = users.Items.map(u => u.email);

  let complete = await Promise.all(users.map(u => {
    let params = {
      EmailAddress: u
    }
    return new Promise((resolve, reject) => {
      SES.verifyEmailIdentity(params, (err, data) => {
        if (err) {
          resolve(null);
        } else {
          resolve(u);
        }
      });
    })
  }));

  return `Successful: ${complete.filter(c => c !== null).legnth}, Filaed: ${complete.filter(c => c === null).length}`;

}

handler();
