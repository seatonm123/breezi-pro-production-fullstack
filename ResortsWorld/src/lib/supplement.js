
export default {

  numberOfDevices(bldgs){
    return bldgs.reduce((a, i) => {
      i.floors.map(f => {
        if (f.rooms) a += f.rooms.length;
      });
      return a;
    }, 0);
  },

  numberOfFloors(bldgs){
    return bldgs.reduce((a, i) => {
      if (i.floors) {
        a += i.floors.length;
      }
      return a;
    }, 0);
  },

  aggregateAlerts(buildings){
    buildings = buildings.map(b => {
      b = getBldgAlerts(b);
      return b;
    });
    let res = {buildings: buildings, alerts: {}, avg: {}};
    res.alerts = buildings.reduce((a, i) => {
      a.bad += i.alerts.bad;
      a.crit += i.alerts.crit;
      return a;
    }, {bad: 0, crit: 0});
    res.avg.days = getAvg(buildings.map(f => f.avg.days));
    res.avg.ttp = getAvg(buildings.map(f => f.avg.ttp));
    res.avg.hhm = getAvg(buildings.map(f => f.avg.hhm));
    res.avg.iaq = getAvg(buildings.map(f => f.avg.iaq));
    res.avg.co2 = getAvg(buildings.map(f => f.avg.co2));
    return res;
  },

  formatRdgs(rooms){
    let serieses = [];
    rooms.map(r => {

    });
  },

  roomAlerts(room){
    let rdgs = {
      days: room.latestDap.values.lifeExpectancy,
      ttp: getAvg(room.latestMeasPacket.dat.ttp),
      hhm: getAvg(room.latestMeasPacket.dat.hhm),
      iaq: getAvg(room.latestMeasPacket.dat.iaq),
      co2: getAvg(room.latestMeasPacket.dat.co2),
    }
    return parseAlerts(rdg);
  }

}

function getBldgAlerts(bldg){

  bldg.floors = bldg.floors.map(f => {
    f = getFloorAlerts(f);
    return f;
  });

  bldg.avg = {days: [], ttp: [], hhm: [], iaq: [], co2: []};
  bldg.alerts = {bad: 0, crit: 0}

  bldg.floors.map(f => {
    if (f.avg) {
      for (let prop in f.avg) {
        bldg.avg[prop].push(f.avg[prop])
      }
    }
    if (f.alerts) {
      bldg.alerts.bad += f.alerts.bad;
      bldg.alerts.crit += f.alerts.crit;
    }
  });

  for (let prop in bldg.avg) {
    bldg.avg[prop] = getAvg(bldg.avg[prop]);
  }
  return bldg;
}

function getFloorAlerts(floor){
  if (floor.rooms && floor.rooms.length > 0) {
    floor.rooms = floor.rooms.map(r => {
      r = getRoomAlerts(r);
      return r;
    });
    floor.avg = {};
    floor.avg.days = getAvg(floor.rooms.map(r => r.avg.days));
    floor.avg.ttp = getAvg(floor.rooms.map(r => r.avg.ttp));
    floor.avg.hhm = getAvg(floor.rooms.map(r => r.avg.hhm));
    floor.avg.iaq = getAvg(floor.rooms.map(r => r.avg.iaq));
    floor.avg.co2 = getAvg(floor.rooms.map(r => r.avg.co2));
    floor.alerts = floor.rooms.reduce((a, i) => {
      a.bad += i.alerts.bad;
      a.crit += i.alerts.crit;
      return a;
    }, {bad: 0, crit: 0});
  }
  return floor;
}


function getRoomAlerts(room) {
  let dat = room.latestMeasPacket.dat;
  let toReturn = {
    avg: {
      days: room.latestDap.values.lifeExpectancy,
      ttp: getAvg(dat.ttp),
      hhm: getAvg(dat.hhm),
      iaq: getAvg(dat.iaq),
      co2: getAvg(dat.co2),
    }
  };
  toReturn.alerts = parseAlerts(toReturn.avg);
  toReturn.name = room.hvacName;
  toReturn.configuration = room.configuration;
  toReturn.tms = room.latestMeasPacket.tms
  return toReturn;
}

function parseAlerts(avg){
  let a = {bad: 0, crit: 0};
  if (avg.days <= 5) {
    a.crit += 1;
  }
  if (avg.days > 5 && avg.days <= 15) {
    a.bad += 1;
  }
  if (avg.ttp <= 0 || avg.ttp >= 29) {
    a.crit += 1;
  }
  if ((avg.ttp >= 10 && avg.ttp <= 18) || (avg.ttp <= 28 && avg.ttp >= 19)) {
    a.bad += 1;
  }
  if (avg.hhm < 20 || avg.hhm > 80) {
    a.crit += 1;
  }
  if (avg.hhm > 40 && avg.hhm < 80) {
    a.bad += 1;
  }
  if (avg.iaq > 250) {
    a.crit += 1;
  }
  if (avg.iaq <= 250 && avg.iaq > 100) {
    a.bad += 1;
  }
  if (avg.co2 >= 1000) {
    a.crit += 1;
  }
  if (avg.co2 < 1000 && avg.co2 >= 350) {
    a.bad += 1;
  }
  return a;
}

function getAvg(arr){
  return arr.reduce((a, i, n) => {
    a += i;
    if (n === arr.length - 1) {
      a = Math.round(a/arr.length);
    }
    return a;
  }, 0);
}
