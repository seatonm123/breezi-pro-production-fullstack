
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'eu-west-1'});
const Cognito = require('amazon-cognito-identity-js');
const cognito = new AWS.CognitoIdentityServiceProvider();
const UserPoolData = {
  UserPoolId: 'eu-west-1_HJnusXcgZ',
  ClientId: 's589kacic8j2dv5n3jfnvgajc'
};

export default {
  login(email, password, newPassword){
    var userPool = new Cognito.CognitoUserPool(UserPoolData);

    var authenticationData = {
        Username: email,
        Password: password
      };
    var authenticationDetails = new Cognito.AuthenticationDetails(authenticationData);

    var userData = {
      Username: email,
      Pool: userPool
    };
    var cognitoUser = new Cognito.CognitoUser(userData);
    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          resolve(result.getAccessToken().getJwtToken());
        },
        onFailure: (result) => {
          reject('login err');
        },
        newPasswordRequired: (userAttributes, requiredAttributes) => {
          if (!newPassword) {
            reject('change password')
          } else {
            cognitoUser.authenticateUser(authenticationDetails, {
          onSuccess: (result) => {
              resolve(result.getAccessToken().getJwtToken());
          },
          onFailure: (result) => {
              reject(null);
          },
          newPasswordRequired: (userAttributes, requiredAttributes) =>{
              if (!newPassword) {
                  reject('change password');
              } else {
                  cognitoUser.completeNewPasswordChallenge(
                      newPassword,
                      {},
                      {
                          onSuccess: (user) => {
                              resolve(user.getAccessToken().getJwtToken());
                          },
                          onFailure: (err) => {
                              reject({code: 401, msg: 'Error logging in with new password'});
                          }
                      }
                  );
              }
                }
            });
          }
        }
      })
    });
  }
}
