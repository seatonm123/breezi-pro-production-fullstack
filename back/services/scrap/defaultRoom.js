
const fs = require('fs');

let type = JSON.stringify({
  id: 'str',
  ac: 'bool',
  aq: 'bool',
  hvacName: 'roomName',
  schema: [],
  configuration: 'equipmentType',
  manufacturer: 'ventManufacturer',
  modelNo: 'ventModelNumber',
  size: 'ventSize',
  brand: 'filterBrand',
  fModel: 'filterModel',
  fType: 'filterType',
  airFilters: [
    {
      fromTimestamp: 'num',
      toTimestamp: 'num',
      id: 'filterId'
    }
  ],
  device: 'deviceId(num)'
}, null, 1)

fs.writeFile('./default.json', type, (err, data) => {
  err ? console.log(err) : console.log('success');
})

let activeFloor = JSON.stringify({
  id: 'floorId',
  index: 'floorIndex(num)',
  name: 'floorName',
  rooms: ['ventId']
}, null, 1);

fs.writeFile('./floor.json', activeFloor, (err, data) => {
  err ? console.log(err) : console.log('success vent');
});
