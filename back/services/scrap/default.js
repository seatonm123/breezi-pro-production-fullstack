
const fs = require('fs');

let type = JSON.stringify({
  id: 'str',
  ac: 'bool',
  aq: 'bool',
  hvacName: 'roomName',
  schema: [],
  configuration: 'equipmentType',
  manufacturer: 'ventManufacturer',
  modelNo: 'ventModelNumber',
  size: 'ventSize',
  brand: 'filterBrand',
  fMode;: 'filterModel',
  fType: 'filterType',
  airFilters: [
    {
      fromTimestamp: 'num',
      toTimestamp: 'num',
      id: 'filterId'
    }
  ],
  device: 'deviceId(num)'
}, null, 1)

fs.writeFile('./default.json', type, (err, data) => {
  err ? console.log(err) : console.log('success');
})
