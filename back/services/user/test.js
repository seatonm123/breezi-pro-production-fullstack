
const Index = require('./index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxNGZkZDJjZi04NTYxLTQ0NGYtYWRmNS00NDQ4ZTFhODNlNzciLCJldmVudF9pZCI6IjY2MTJjYzI0LTc2MjMtNDMzMC1iNWNhLTQzYTI4YjBjMDEzZiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzE3MjI3MDcsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3MTcyNjMwNywiaWF0IjoxNTcxNzIyNzA3LCJqdGkiOiI4YTJmNzU2Zi0xY2U4LTQ3ZmQtODVhYy03YzU0MzQzN2ZjNzUiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiIxNGZkZDJjZi04NTYxLTQ0NGYtYWRmNS00NDQ4ZTFhODNlNzcifQ.I0c3bjtak1CKg_kKTXF5BfgsOvIDg4d3EW18zGwtg9wydeS085q8lXxbyyLIVGNtzzkmAzmVOObCJPNnXvL5BA8BmeCSn9OmHxIbtfiukpE-RG243JKauVrX1IcJaQgQoQuSQoQgu5pUbpTuKATh7QMrLE-JeGS7FWZLu1BhDtuSibddaIMKVW71oks4kmpnX-KLkYYIvX7m-q8oi7kVH0fXI2RbhgrMG8nFM2O_8Jx_O9jgNQ0TAon_CuV3mZN9C-QlrAA9PUl7ttzEgY2fX3unzePQ32cjz2kx2U6ugpQg92FQLX0NY_4dCJJo_VpkokVl_gYcC0icnI82s6ZjIA`;

const TestEvent = {
  httpMethod: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': tkn
  },
  // body: JSON.stringify({
  //   firstName: 'Robert',
  //   lastName: 'Seaton',
  //   email: 'matt.r.seaton@gmail.com',
  //   phoneNumber: '+12128675309'
  // })
}

Index.handler(TestEvent, '', (err, res) => {
  err ? console.log({ERROR: err}) : console.log(JSON.stringify(res, null, 1))
});
