
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const SES = new AWS.SES();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      handleHttpRequest(event, admin, org);
    });

  function handleHttpRequest(event, admin, org){

    switch (event.httpMethod) {
      case 'GET':
        handleGet(event, admin, org);
      break;
      case 'POST':
        handlePost(event, admin, org);
      break;
      case 'PUT':
      break;
      case 'DELETE':
      break;
    }

  }

  function handleGet(event, admin, org){
    getTeam(admin, org);
  }

  function getTeam(admin, org){
    let admins = org.admins;
    admins.push(org.masterAdmin);
    Promise.all(admins.map(a => {
      return new Promise(resolve => {
        Dynamo.getItem({
          TableName: 'Users-matt',
          Key: {
            email: a
          }
        }, (err, data) => {
          if (err || !data.Item || !data.Item.email) {
            resolve(null);
          } else {
            let user = data.Item;
            delete user.admin;
            delete user.confirmed;
            delete user.org;
            delete user.buildings;
            resolve(user);
          }
        });
      })
    })).then(users => {
      users = users.filter(u => u !== null);
      response(200, {
        org: org,
        users: users,
        me: admin.email
      });

    });
  }

  function handlePost(event, admin, org){
    let validBody = validatePost(event.body, admin);
    if (validBody.code) {
      response(validBody.code, validBody.msg)
    }
    console.log(validBody);
    addCognitoUser(validBody, admin)
      .then(cUser => {
        addDynamoUser(cUser);
      }, cErr => {
        response(cErr.code, cErr.msg);
      });
  }

  function validatePost(body, admin){
    let json = Core.parseFromJson(body, 'email');
    if (!json.email) {
      return {code: 403, msg: json}
    }
    function hasProp(prop){
      return json[prop];
    }
    if (!['email', 'firstName', 'lastName'].every(hasProp)) {
      return {code: 403, msg: 'New user email, firstName and lastName must be included in request body'};
    }

    json.admin = true;
    json.masterAdmin = false;
    json.id = uuid();
    json.org = admin.org;
    json.buildings = [];
    json.confirmed = true;

    return json;

  }

  function addCognitoUser(user, admin){
    const poolId = 'eu-west-1_HJnusXcgZ';
    let params = {
      DesiredDeliveryMediums: ['EMAIL'],
      ForceAliasCreation: false,
      MessageAction: 'SUPPRESS',
      UserAttributes: [
        {
          Name: 'email',
          Value: user.email
        },
        {
          Name: 'email_verified',
          Value: 'True'
        }
      ],
      Username: user.email,
      UserPoolId: poolId,
      ValidationData: [
        {
          Name: 'email',
          Value: user.email
        }
      ]
    };
    let resendParams = {
      DesiredDeliveryMediums: ['EMAIL'],
      ForceAliasCreation: false,
      MessageAction: 'RESEND',
      UserAttributes: [
        {
          Name: 'email',
          Value: user.email
        },
        {
          Name: 'email_verified',
          Value: 'True'
        }
      ],
      Username: user.email,
      UserPoolId: poolId,
      ValidationData: [
        {
          Name: 'email',
          Value: user.email
        }
      ]
    }
    return new Promise((resolve, reject) => {
      Cognito.adminCreateUser(params, (err, data) => {
        if (err) {
          reject({code: err.statusCode ? err.statusCode : 400, msg: err.message ? err.message : 'Error adding user to user pool'});
        } else {
          Cognito.adminCreateUser(resendParams, (rErr, rData) => {
            if (rErr) {
              reject({code: rErr.statusCode ? rErr.statusCode : 400, msg: rErr.message ? rErr.message : 'Error sending invitation to user ' + user.email});
            } else {
              resolve(user);
            }
          });
        }
      });
    });
  }

  function addDynamoUser(user){
    Dynamo.putItem({
      TableName: 'Users-matt',
      Item: user
    }, (err, data) => {
      if (err) {
        response(err.statusCode ? err.statusCode : 400, err.message ? err.message : `Error adding user ${user.email} to DB`);
      } else {
        addUserToOrg(user);
      }
    });
  }

  function addUserToOrg(user){
    let org = user.org;
    Dynamo.getItem({
      TableName: 'Organizations-matt',
      Key: {
        name: org
      }
    }, (err, data) => {
      if (err) {
        response(500, 'Error adding user to organization ' + org);
      } else if (!data.Item || !data.Item.name) {
        response(400, `Organization ${org} not found in database. Failed attempting to add user to organization`);
      } else {
        let orgAdmins = data.Item.admins;
        orgAdmins.push(user.email);
        Dynamo.updateItem({
          TableName: 'Organizations-matt',
          Key: {
            name: data.Item.name
          },
          UpdateExpression: 'SET admins = :orgAdmins',
          ExpressionAttributeValues: {
            ':orgAdmins': orgAdmins
          }
        }, (uErr, uData) => {
          if (uErr) {
            response(400, `Error adding user ${user.email} to org ${data.Item.name}`);
          } else {
            response(200, `User ${user.email} was successfully added to organization ${user.org}`);
            // verifySESUser(user.email);
          }
        });
      }
    });
  }

  function verifySESUser(email){
    let params = {EmailAddress: email};
    SES.verifyEmailIdentity(params, (err, data) => {
      if (err) {
        response(400, `User ${email} has been added to user pool and db, but was not added to verified SES email addresses`);
      } else {
        response(200, `User ${email} successfully added to org`);
      }
    });
  }

};
