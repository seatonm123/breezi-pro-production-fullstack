const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let BUILDINGS = `Buildings${process.env.SUFFIX || '-matt'}`;
let DEVICES = `Devices${process.env.SUFFIX || '-matt'}`;
let MEAS = `Devices${process.env.SUFFIX || '-matt'}`;
let DAPS = `Devices${process.env.SUFFIX || '-matt'}`;

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb (null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    })
  };

  let tkn = event.headers.Authorization || null;

  if (event.httpMethod !== 'GET') {
    response(405, 'Http method not allowed');
  }

  Core.getUserOrg(tkn)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      if (!event.pathParameters) {
        getAllData(admin, org);
      } else {
        if (event.queryStringParameters.floor) {
          getFloorData(event.pathParameters.buildingId, event.queryStringParameters.floor, admin, org);
        }
      }
    })

  // Core.getUserOrg(tkn)
  //   .then(details => {
  //     let admin = details.user;
  //     let org = details.org;
  //     if (event.resource === '/bundle/{buildingId}') {
  //       if (event.queryStringParameters && event.queryStringParameters.floor) {
  //         getFloorData(event.pathParameters.buildingId, event.queryStringParameters.floor, admin, org);
  //       } else {
  //         getBldgData(event.pathParameters.buildingId, admin, org);
  //       }
  //     } else {
  //       getAllData(admin, org);
  //     }
  //   });

  function getFloorData(bId, fIndex, admin, org){
    fIndex = parseInt(fIndex);
    Dynamo.getItem({
      TableName: BUILDINGS,
      Key: {
        id: bId
      }
    }, (err, data) => {
      if (err) {
        response(err.statusCode ? err.statusCode : 400, err.message ? err.message : err);
      } else {
        if (!data.Item || !data.Item.id) {
          response(400, `Building ${bId} not found`);
        }
        let bldg = data.Item;
        if (bldg.org !== org.name) {
          response(401, `Building ${bId} belongs to a different organization`);
        }
        let thisFloor = bldg.floors.active.find(f => f.index === fIndex);
        if (thisFloor === undefined) {
          response(400, `Floor ${fIndex} is currently inactive.`);
        }
        thisFloor.rooms = thisFloor.rooms.map(r => {
          r = bldg.airVents.find(v => v.id === r);
          return r;
        });
        response(200, thisFloor);
      }
    });
  }

  // function getBldgData(){
  //
  // }
  //
  function getAllData(admin, org){
    if (org.buildings.length < 1) {
      response(200, {admin: admin, organization: org, buildings: []});
    } else {
      Promise.all(org.buildings.map(b => {
        return new Promise((resolve, reject) => {
          Dynamo.getItem({
            TableName : 'Buildings-matt',
            Key: {
              id: b
            }
          }, (err, data) => {
            if (err || !data.Item || !data.Item.id) {
              resolve(null);
            } else {
              resolve(data.Item);
            }
          })
        })
      })).then(bldgs => {
        bldgs = bldgs.filter(b => b !== null);
        response(200, {
          admin: admin,
          organization: org,
          buildings: bldgs
        });
      });
    }
  }

}
