
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-matt';

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb (null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': process.env.ORIGIN || '*'
      }
    })
  };

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'GET') {
    response(405, 'Http method not allowed');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      getData(admin, org, event)
        .then(res => {
          response(200, res);
        }, err => {
          if (err === 418) {
            response(200, {admin: admin, organization: org, buildings: []});
          }
        });
    });

  function getData(admin, org, event){

    return new Promise((resolve, reject) => {

      if (!event.pathParameters) {
        rawCall(admin, org);
      }

      if (event.resource === '/buildings/{buildingId}') {
        if (event.queryStringParameters && event.queryStringParameters.floor) {
          getOneFloor(event, admin, org);
        }
      }


    });

  }

  function rawCall(admin, org) {
    return new Promise((resolve, reject) => {
      if (org.buildings.length < 1) {
        reject(418);
      } else {
        Promise.all(org.buildings.map(b => {
          return new Promise((resolve, reject) => {
            Dynamo.getItem({
              TableName : 'Buildings-matt',
              Key: {
                id: b
              }
            }, (err, data) => {
              if (err || !data.Item || !data.Item.id) {
                resolve(null);
              } else {
                resolve(data.Item);
              }
            })
          })
        })).then(bldgs => {
          bldgs = bldgs.filter(b => b !== null);
          response({
            admin: admin,
            organization: org,
            buildings: bldgs
          });
        });
      }
    });
  }

}
