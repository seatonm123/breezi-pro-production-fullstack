
const Index = require('./building/index');

const Floor = require('./floor/index');

const Bundle = require('./bundle/index');

const Room = require('./room/index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI0OTNjM2VmYi00Zjc1LTRjNmEtYmUwNy02ZTNmODVjMmQ0MzkiLCJldmVudF9pZCI6Ijc3YTNkZTZiLTQ4YWItNDFkZC1hOWNiLWEzMGYyMjIxYmY3YSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NjY2MTczNjcsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU2NjYyMDk2NywiaWF0IjoxNTY2NjE3MzY4LCJqdGkiOiJmNWMwN2ZkYy01MDNjLTRjODQtOWI0OC03OGZhYjg1NzJkNTIiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiI0OTNjM2VmYi00Zjc1LTRjNmEtYmUwNy02ZTNmODVjMmQ0MzkifQ.IltC5Jk-pQLAAPke8pYaM7WaEEOcd8T-CDfu1b4lVGKSau4q15YnW5yD2xZ-23ntYIshhoGIuIgLY_IGtZyeiqClPrS8gyXYNr3QOn_ZNks4ytyDcbbCLY9VmLYim_Fvc-2iDXxdiC8dnkH9hM_CuInzjwX4I9p_pqOcYL3QE2cFxjR6gxabI2oWJMXBmygUuxqchEdS1YpU-2VfG0rdjmRzej9xI3UwvlHwoK73LsJ6u5BpYqRZDgxHkfaASuxrJQTkcfYZhKIqkpeTUtigBMgKTIugeEy9owI7MHJwxs-PPOQDKO5ihXd3-2b65X2BNZcAgX_jww9WDqiqiuYQVQ`;

const TestEvent = {
  httpMethod: 'POST',
  headers: {
    'Authorization': tkn,
    'Content-Type': 'application/json'
  },
  resource: '/buildings/{buildingId}',
  pathParameters: {
    buildingId: 'b10e8448-f201-4dd1-8156-817d5571f3ae'
  },
  body: JSON.stringify({
    name: 'Englewood Villas',
    street: '8031 Broadway',
    streetExtended: ' ',
    city: 'Vancouver',
    state: 'BC',
    zipCode: '11116-226',
    country: 'CA',
    aboveGround: 7,
    belowGround: 2
  })
};

const FloorEvent = {
  httpMethod: 'POST',
  headers: {
    'Authorization': tkn,
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    name: 'Penthouse',
    index: 7,
    id: 'a9912a05-442a-4839-a0a7-23499a465c97',
    bId: '25bcd46a-e8b0-441e-9efd-0d6fdcba660f'
  })
}

const BundleEvent = {
  httpMethod: 'GET',
  headers: {
    'Authorization': tkn,
    'Content-Type': 'application/json'
  },
  resource: '/buildings/{buildingId}',
  pathParameters: {
    buildingId: '9c4b4458-f657-431e-b089-ecc438569291'
  },
  queryStringParameters: {
    floor: '11'
  }
}


const RoomEvent = {
  httpMethod: 'POST',
  headers: {
    'Authorization': tkn,
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
   ac: false,
   aq: true,
   hvacName: 'Room 301',
   // configuration: 'fcu',
   // manufacturer: 'Rider',
   // modelNo: '31415926536a',
   // size: '20x13x1',
   // brand: 'Filtrete',
   // fModel: 'XJ220',
   // fType: 'HEPA filter',
   airFilters: [
   ],
   device: 123456,
   floorIndex: 11,
   buildingId: 'e0f1bc4d-c941-4187-9dce-1b6f150d4f92',
   floorName: 'Penthouse',
   hardwareVersion: 'APP1R1'
 })
}



// Index.handler(TestEvent, '', (err, res) => {
//   err ? console.log(err) : console.log(JSON.stringify(res, null, 1));
// })


// Floor.handler(FloorEvent, '', (err, res) => {
//   err ? console.log(err) : console.log(JSON.stringify(res, null, 1));
// });

Bundle.handler(BundleEvent, '', (err, res) => {
  err ? console.log(err) : console.log(JSON.stringify(res, null, 1));
});

// Room.handler(RoomEvent, '', (err, res) => {
//   err ? console.log(err) : console.log(JSON.stringify(res, null, 1));
// });
