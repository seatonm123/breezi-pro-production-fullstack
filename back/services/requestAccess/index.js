
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-matt';

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : ' ',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  };

  if (event.httpMethod !== 'POST') {
    response(405, 'Http method not allowed');
  }

  let body = parseFromJson(event.body, 'user');

  if (body === false) {
    response(403, 'Event body must be present and in valid JSON format');
  }

  requestAccess(body);

  function requestAccess(userOrg){

    let validCreds = validateUserCreds(userOrg);
    checkIfEntityExists(userOrg.org, 'Organizations', 'name')
      .then(validOrg => {
        checkIfEntityExists(userOrg.user, 'Users', 'email')
          .then(validUser => {
            persistUserAndOrg(validOrg, validUser)
              .then(newUser => {
                alertBreezi(newUser);
              }, newErr => {
                response(400, newErr);
              });
          }, userErr => {
            response(400, userErr);
          });
      }, orgErr=> {
        response(400, orgErr);
      });

  }

  function validateUserCreds(userOrg){
    if (!userOrg.user || !userOrg.org) {
      response(403, 'Request body must consist of a user and an organization');
      return;
    }
    let user = userOrg.user;
    let org = userOrg.org;
    function checkUser(item){
      return user[item];
    }
    if (!['firstName', 'lastName', 'email', 'phoneNumber'].every(checkUser)) {
      response(403, 'USER firstName, lastName, email and phone number must be included in request body');
      return;
    };
    function checkOrg(item){
      return org[item];
    }
    if (!['street', 'city', 'state', 'zipCode', 'country', 'name'].every(checkOrg)) {
      response(403, 'ORGANIZATION street, city, state, zip code, country and name must be included in request body');
      return;
    };
    return userOrg;
  }

  function checkIfEntityExists(item, table, prop){
    return new Promise((resolve, reject) => {
      let params = {
        TableName: table + SUFFIX,
        Key: {}
      };
      params.Key[prop] = item[prop];
      Dynamo.getItem(params).promise()
        .then(i => {
          let prop = table === 'Organizations' ? 'name' : 'email';
          if (!i.Item || !i.Item[prop]) {
            resolve(item);
          } else {
            reject(`${table.substr(0, table.length - 1)} already exists`);
          }
        });
    });
  }

  function persistUserAndOrg(org, user){
    user.confirmed = false;
    user.id = uuid();
    user.buildings = [];
    user.masterAdmin = true;
    user.admin = false;
    org.id = uuid();
    org.masterAdmin = user.email;
    org.admins = [];
    org.buildings = [];
    user.org = org.name;
    return new Promise((resolve, reject) => {
        Dynamo.putItem({
          TableName: 'Organizations' + SUFFIX,
          Item: org
        }).promise().then(() => {
          Dynamo.putItem({
            TableName: 'Users' + SUFFIX,
            Item: user
          }).promise().then(() => {
            resolve({org: org, user: user});
          }).catch(userErr => {
            response(400, userErr);
          });
        }).catch(orgErr => {
          reject(400, orgErr);
        })
    });
  }

  function alertBreezi(details){
    let user = details.user;
    let org = details.org;
    const matt = 'matt.r.seaton@breezi.io';
    const tommi = 'tommi@breezi.io';
    const tim = 'tim@breezi.io';
    const title = `${user.email} has requested access to Breezi Pro`
    let body =
      `<h2>A user with the email ${user.email} has requested access to Breezi Home on behalf of ${org.name}</h2>
      <h3>User Info</h3>
      <ul>
        <li><b>Name</b>: ${user.firstName} ${user.lastName}</li>
        <li><b>Email</b>: ${user.email}</li>
        <li><b>Phone number</b>: ${user.phoneNumber}</li>
        <li><b>Id</b>: ${user.id}</li>
      </ul>
      <h3>Organization Info</h3>
      <ul>
        <li><b>Name</b>: ${org.name}</li>
        <li><b>Address</b>: ${org.street}${org.streetExtended ? ' ' + org.streetExtended : ''} <br/> ${org.city}, ${org.state} ${org.zipCode} <br/> ${org.country}</li>
      </ul>
      <br/>
      <p>
        Please email Matt S at matt.r.seaton@breezi.io if this user meets approval standards and he will add the user to the User Pool.

        -Breezi Probot
      </p>`
    let params = {
      Destination: {
        ToAddresses: [
          matt,
          // tommi,
          // tim
        ]
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: body
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: title
        }
      },
      Source: matt,
      ReplyToAddresses: [
        matt
      ]
    };

    let sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
    sendPromise.then(data => {
      response(200, 'Messages sent to Matt, Tim and Tommi');
    }).catch(err => {
      response(400, err);
    });

  }

  function parseFromJson(item, prop){
    if (item[prop]) {
      return item;
    } else if (JSON.parse(item)[prop]) {
      return JSON.parse(item);
    } else {
      return false;
    }
  }

};
