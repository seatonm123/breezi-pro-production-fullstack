
const Index = require('./index');

const body = JSON.stringify({
  user: {
    email: 'matt.r.seaton@breezi.io',
    firstName: 'Matt',
    lastName: 'Seaton',
    phoneNumber: '+19199997195'
  },
  org: {
    street: '5914 S Datura St',
    streetExtended: 'Suite 10',
    city: 'Littleton',
    state: 'US-CO',
    zipCode: '80210',
    country: 'US',
    name: 'ASPCA'
  }
});

const TestEvent = {
  httpMethod: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: body
};

Index.handler(TestEvent, {}, (err, res) => {
  if (err) console.log({ERROR: err});
  console.log(JSON.stringify(res, null, 1));
})
