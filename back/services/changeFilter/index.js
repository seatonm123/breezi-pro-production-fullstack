
const Core = require('./core');

const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const SUFFIX = process.env.SUFFIX || '-matt';

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg), null, 1);
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'POST') {
    response(405, 'HTTP method not allowed');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(async details => {
      const admin = details.user;
      const org = details.org;
      let body = Core.parseFromJson(event.body, 'airVentId');
      if (!body.airVentId || !body.buildingId) {
        response(400, body);
      }
      let validVent = await validateVent(org, body);
      if (!validVent.bldg) {
        response(400, validVent);
      }
      console.log(validVent.bldg);
      Dynamo.updateItem({
        TableName: `Buildings${SUFFIX}`,
        Key: {
          id: validVent.bldg.id
        },
        UpdateExpression: 'SET airVents = :vents',
        ExpressionAttributeValues: {
          ':vents': validVent.bldg.airVents
        }
      }, (updateErr, data) => {
        if (updateErr) {
          console.log(JSON.stringify(updateErr, null, 1));
          response(400, `Could not update building ${validVent.bldg.name}`);
        }
        console.log(`Update persisted in DB`);
        response(200, `Building ${validVent.bldg.name} has been updated with a new filter`);
      })
    }, err => {
      response(err.code, err.msg);
    });

  function validateVent(org, body){
    return new Promise(async resolve => {
      let v = body.airVentId;
      let b = body.buildingId;
      let bldg = await Dynamo.getItem({
        TableName: `Buildings${SUFFIX}`,
        Key: {
          id: b
        }
      }).promise();
      if (!bldg.Item || !bldg.Item.id) {
        resolve (`Building ${b} could not be located in database`);
      }
      bldg = bldg.Item;
      let inBldg = isInBldg(bldg, v);
      if (!inBldg.vent) {
        resolve(inBldg);
      }

      let thisVent = bldg.airVents.find(v => v.id === inBldg.vent.id);
      let ventIndex = bldg.airVents.indexOf(thisVent);
      bldg.airVents[ventIndex].airFilters = inBldg.vent.airFilters;

      console.log({BUILDING_UPDATED: JSON.stringify(bldg, null, 1)});
      resolve({bldg, bldg});
    })
  }

  function isInBldg(bldg, ventId) {
    let vent = bldg.airVents.find(v => v.id === ventId);

    if (vent === undefined) {
      return `AirVent ${ventId} is not located in building ${bldg.name}`;
    }
    console.log(JSON.stringify({
      msg: 'VENT VALID',
      data: vent
    }, null, 1));
    let now = Math.floor(Date.now()/1000);
    let filters = vent.airFilters.filter(f => !f.toTimestamp);
    if (filters.length > 0) {
      filters = filters.map(f => {
        if (!f.toTimestamp) {
          f.toTimestamp = now;
        }
        return f;
      });
    }
    vent.airFilters.push({
      id: uuid(),
      fromTimestamp: now
    });

    console.log(JSON.stringify({
      msg: 'VENT UPDATED',
      data: vent
    }, null, 1));

    return ({vent: vent});

  }

};
