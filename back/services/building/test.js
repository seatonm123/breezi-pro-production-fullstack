
const Index = require('./index');
const tkn =  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI0OTNjM2VmYi00Zjc1LTRjNmEtYmUwNy02ZTNmODVjMmQ0MzkiLCJldmVudF9pZCI6IjIyZmU5MTk1LTI0MTQtNDE1Mi1hNDQ5LWY2NzgwNmQ5N2IwNyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzE3ODkzOTAsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3MTc5Mjk5MCwiaWF0IjoxNTcxNzg5MzkwLCJqdGkiOiI2ZTg2MWE5MS0zNzJlLTQ1ZGItODkzZi1mY2IwYWI5NmViYmUiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiI0OTNjM2VmYi00Zjc1LTRjNmEtYmUwNy02ZTNmODVjMmQ0MzkifQ.q0Yo4XJPtO756p4ZbN7xoezWJ7b2NCAJ9S2xAXS8libWg0_PUQ3jnhtif3Tp5KPfNv35ceKX6wa0giJB4oqQmw7xKyteDuRbEF-YaXQ9dA5hsT8ndHCRcilAsbNAUUT6ORKd4-OM6p1yol47mYEKcBAtEXSigc1vX5UWiF4T1FMJW3zboDXm3si22KiCD0LvJPmvCghM_Tm94TwTFPUyaqy9EzbjcGVo6Kz3DY-MZTmQHIt5RMzIVNpV90T6uva5I35VYOxqiABZDpXDlBSKMpwlXyTvSRE8Vntmwuq7lRKra5w2iti35DGjw9Bzn2hDEdAa6khATraXzkuHGHUCMQ`;

const TestEvent = {
  httpMethod: 'DELETE',
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  },
  resource: '/buildings/{buildingId}',
  pathParameters: {
    buildingId: 'd159edd3-933f-486d-94a9-9a89ee118b02'
  }
}

Index.handler(TestEvent, {}, (err, data) => {
  err ? console.log({ERROR: err}) : console.log(data);
});
