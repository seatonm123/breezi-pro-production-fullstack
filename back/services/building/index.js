
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const Core = require('./core');

let BUILDINGS = `Buildings-${process.env.SUFFIX || 'matt'}`;
let ORGANIZATIONS = `Organizations-${process.env.SUFFIX || 'matt'}`;

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  let tkn = event.headers.Authorization || null;

  Core.getUserOrg(tkn)
    .then(details => {
      const admin = details.user;
      const org = details.org;
      let bldg = null;
      if (event.httpMethod !== 'GET' && event.httpMethod !== 'DELETE') {
        bldg = validateBldgObj(Core.parseFromJson(event.body ? event.body : null, 'street')) || null;
        if (!bldg.street) {
          response(403, bldg);
        }
      }
      handleHttpRequest(event, bldg, admin, org);
    }, coreErr => {
      response(coreErr.code, coreErr.msg);
    });

  function validateBldgObj(bldg){

    function hasProp(prop){
      return bldg[prop];
    }
    if (!['street', 'city', 'state', 'zipCode', 'country', 'aboveGround', 'belowGround'].every(hasProp)) {
      return 'Building object must contain street, city, state, zipCode, country, aboveGround and belowGround properties';
    }
    if (!bldg.name) {
      bldg.name = `${bldg.street}${bldg.streetExtended ? ' ' + bldg.streetExtended : ''} ${bldg.city}, ${bldg.state} ${bldg.zipCode}, ${bldg.country}`
    }
    return bldg;

  }

  function handleHttpRequest(event, bldg, admin, org){

    let bId = event.resource === '/buildings/{buildingId}'
      ? event.pathParameters.buildingId
      : null;

    switch (event.httpMethod) {
      case 'POST':
        handlePost(bldg, admin, org);
      break;
      case 'PUT':
        handlePut(bldg, admin, org, bId);
      break;
      case 'DELETE':
        handleDelete(admin, org, bId);
      break;
      default:
        response(405, 'Http method not allowed');
    }

  }

  function handlePost(bldg, admin, org){
    bldg.id = uuid();
    bldg.org = org.name;
    bldg.admins = [admin.email];
    bldg.floors = {
      aboveGround: bldg.aboveGround,
      belowGround: bldg.belowGround,
      active: []
    };
    bldg.airVents = [];
    delete bldg.aboveGround;
    delete bldg.belowGround;
    Dynamo.putItem({
      TableName: BUILDINGS,
      Item: bldg
    }, (err, data) => {
      if (err) {
        response(400, err.message ? err.message : err);
      } else {
        persistOrgUpdate(bldg.id, org);
      }
    })
  }

  function handlePut(bldg, admin, org, id){

    validateUpdateDelete(admin, org, id)
      .then(existingBldg => {
        for (let prop in bldg) {
          if (!existingBldg[prop] || existingBldg[prop] !== bldg[prop]) {
            existingBldg[prop] = bldg[prop];
          }
        }
        Dynamo.putItem({
          TableName: BUILDINGS,
          Item: existingBldg
        }, (err, data) => {
          if (err) {
            response(err.statusCode ? err.statusCode : 400, err.message ? err.message : err);
          } else {
            response(200, `Building ${id} has been updated`);
          }
        });
      }, exErr => {
        response(exErr.code, exErr.msg);
      })

  }

  function handleDelete(admin, org, id){

    validateUpdateDelete(admin, org, id)
      .then(bldg => {
        let devices = bldg.airVents.map(v => v.device);
        Promise.all(devices.map(d => {
          Dynamo.deleteItem({TableName: 'Devices-matt', Key: {deviceId: d}}, (err, data) => {
            err ? console.log({DELETE_DEVICE_ERROR: d}) : console.log({DELETE_DEVICE_SUCCESS: d});
            return Promise.resolve(null);
          })
        })).then(noDevs => {
          Dynamo.deleteItem({
            TableName: BUILDINGS,
            Key: {
              id: id
            }
          }, (err, data) => {
            if (err) {
              response(400, `Unable to delete building ${id}`);
            } else {
              persistOrgDelete(org, id);
            }
          });
        }, vErr => {
          response(vErr.code, vErr.msg);
        });
      })

  }

  function persistOrgUpdate(bId, org){
    org.buildings.push(bId);
    let params = {
      TableName: ORGANIZATIONS,
      Key: {
        name: org.name
      },
      UpdateExpression: 'set buildings = :bldgs',
      ExpressionAttributeValues: {
        ':bldgs': org.buildings
      }
    }
    Dynamo.updateItem(params, (err, data) => {
      if (err) {
        response(500, 'Could not persist organization update. Building was added but not associated with Organization. Please delete building and try again');
      } else {
        response(200, `Building ${bId} has been added to the database`);
      }
    })
  }

  function persistOrgDelete(org, id){
    org.buildings = org.buildings.filter(o => o !== id);
    let params = {
      TableName: ORGANIZATIONS,
      Key: {
        name: org.name
      },
      UpdateExpression: 'set buildings = :bldgs',
      ExpressionAttributeValues: {
        ':bldgs': org.buildings
      }
    }
    Dynamo.updateItem(params, (err, data) => {
      if (err) {
        response(400, `Organization ${org.name} could not be updated. Building ${id} has been deleted but reference to deleted building still exists in db`);
      } else {
        response(200, `Building ${id} has been successfully deleted`);
      }
    });
  }

  function validateUpdateDelete(admin, org, id){
    return new Promise((resolve, reject) => {
      if (id === null) {
        reject({code: 403, msg: 'The id of the building to be updated must be included in the request path parameters'});
      }
      Dynamo.getItem({
        TableName: BUILDINGS,
        Key: {
          id: id
        }
      }).promise()
        .then(existingBldg => {
          if (!existingBldg || !existingBldg.Item || !existingBldg.Item.id) {
            reject({code: 400, msg: 'Building not found in database'});
          } else {
            existingBldg = adminCanUpdate(existingBldg.Item, org);
            if (!existingBldg.id) {
              reject({code: 400, msg: existingBldg});
            } else {
              resolve(existingBldg);
            }
          }
        });
    });
  }

  function adminCanUpdate(bldg, org){
    if (org.buildings.indexOf(bldg.id) < 0) {
      return 'This building is owned by a different organization';
    } else {
      return bldg;
    }
  }

}
