'use strict';

const doc = require('dynamodb-doc');
const Common = require('brebe-common');

const dynamo = new doc.DynamoDB();
const common = new Common();

// const TABLE_BUILDINGS = "Buildings" + process.env.ENVIRONMENT_SUFFIX;
// const TABLE_MEASUREMENTS = "Measurements" + process.env.ENVIRONMENT_SUFFIX;
// const TABLE_DEVICES = "Devices" + process.env.ENVIRONMENT_SUFFIX;

const TABLE_BUILDINGS = 'Buildings-matt';
const TABLE_MEASUREMENTS = 'Measurements-matt';
const TABLE_DEVICES = 'Devices-matt'

const AIR_FILTER_ID_TMS_INDEX = "airFilterId-tms-index";

exports.handler = (event, context, callback) => {

    const response = (httpCode, message) => {
        callback(null, {
            statusCode: httpCode,
            body: httpCode !== 200 ? "" : JSON.stringify(message),
            headers: {
                'Content-Type': 'application/json',
            }
        });
    };

    common
        .getUserFromToken(event.headers.Authorization)
        .then(userItem => {
            handleHttpRequest(event, userItem.Item);
        })
        .catch(err => {
            console.log("Error while processing MeasurementData request", err);
            if (err instanceof SyntaxError) {
                response(400);
            }
            else {
                response(500);
            }
        });

    function handleHttpRequest(event, user) {
        switch (event.httpMethod) {
            case "GET":
                get(event, user);
                break;
            case "POST":
                post(event, user);
                break;
            default:
                console.log(`Unsupported HTTP method ${event.httpMethod}`);
                response(500);
        }
    }

    function get(event, user) {
        let airFilterId = event.pathParameters ? event.pathParameters.airFilterId : false;
        if (!airFilterId) {
            response(400);
            return;
        }

        // dynamo.scan({
        //         TableName: TABLE_BUILDINGS,
        //         FilterExpression: "#userId = :userId",
        //         ExpressionAttributeNames: {
        //             "#userId": "userId",
        //         },
        //         ExpressionAttributeValues: {
        //             ":userId": user.id,
        //         }
        //     }).promise()
        dynamo.getItem({
              TableName: TABLE_BUILDINGS,
              Key: {
                id: user.buildingId[0],
                userId: user.id
              }
            }).promise()
            .then(buildingItem => {
                if (!buildingItem.Item) {
                    console.log(`No Building for User ${user.id}`);
                    response(500);
                    return;
                }

                let building = buildingItem.Item;

                if (!building.airVents || building.airVents.length === 0) {
                    console.log(`No AirVents in Building ${building.id}`);
                    response(400);
                    return;
                }

                if (!airFilterInBuilding(building, airFilterId)) {
                    console.log(`Building ${building.id} does not have AirFilter ${airFilterId}`);
                    response(400);
                    return;
                }

                let activeFilter = building.airVents.reduce((accum, iter) => {
                  if (iter.airFilters && iter.airFilters.length > 0) {
                    var filter = iter.airFilters.find(f => f.id === airFilterId);
                    if (filter !== undefined) {
                      accum = filter;
                      accum.vent = iter.id;
                      accum.device = iter.deviceId;
                    }
                  }
                  return accum;
                }, {});

                console.log(activeFilter.fromTimestamp);

                let filterTms = activeFilter.fromTimestamp
                  ? activeFilter.fromTimestamp
                  : null;

                  console.log(filterTms);

                if (filterTms === null) {
                  console.log(`Filter ${airFilterId} was not found`);
                  response(400);
                  return;
                }

                // let queryParams = {
                //     TableName: TABLE_MEASUREMENTS,
                //     IndexName: AIR_FILTER_ID_TMS_INDEX,
                //     KeyConditionExpression: "airFilterId = :airFilterId",
                //     ScanIndexForward: false,
                //     ExpressionAttributeValues: {
                //         ":airFilterId": airFilterId
                //     }
                // };

                let queryParams = {
                  TableName: TABLE_MEASUREMENTS,
                  KeyConditionExpression: 'deviceId = :did AND tms >= :tms',
                  ExpressionAttributeValues: {
                    ':did': activeFilter.deviceId,
                    ':tms': activeFilter.fromTimestamp
                  },
                  ScanIndexForward: false,
                  Limit: 1
                }

                let limit = event.queryStringParameters ? event.queryStringParameters.limit : false;
                if (limit) {
                    if (isNaN(limit)) {
                        response(400);
                        return;
                    }
                    queryParams.Limit = parseInt(limit, 10);
                }
                queryMeasurements(queryParams, []);
            })
            .catch(err => {
                console.log(`Error while querying Building for User ${user.id}`, err);
                response(500);
            });
    }

    function queryMeasurements(queryParams, measurements) {
        dynamo.query(queryParams)
            .promise()
            .then(measurementsItem => {
                // if (shouldQueryAdditionalMeasurementData(measurements, queryParams, measurementsItem)) {
                //     queryParams.ExclusiveStartKey = measurementsItem.LastEvaluatedKey;
                //     measurements.push(...measurementsItem.Items);
                //     queryMeasurements(queryParams, measurements);
                // }
                // else {
                //     if (queryParams.Limit) {
                //         measurements.push(...measurementsItem.Items.slice(0, queryParams.Limit - measurements.length));
                //     }
                //     else {
                //         measurements.push(...measurementsItem.Items);
                //     }
                //     response(200, measurements.map(mapMeasurementDataToDTO));
                // }

                response(200, measurements.map(mapMeasurementDataToDTO));

            })
            .catch(err => { throw new Error(err); });
    }

    // function shouldQueryAdditionalMeasurementData (measurements, queryParams, measurementsItem) {
    //     return (measurementsItem.LastEvaluatedKey && !queryParams.Limit) || (queryParams.Limit && measurements.length < queryParams.Limit && measurementsItem.LastEvaluatedKey);
    // }

    function post(event, user) {
      let measurementsData = event.body;
        // let measurementsData = JSON.parse(event.body);
        if(!Array.isArray(measurementsData)) {
            console.log(`Invalid MeasurementData payload. Payload should be an array`, measurementsData);
            response(400);
            return;
        }
        measurementsData.forEach(measurementData => persistMeasurementData(measurementData, user));
    }

    function persistMeasurementData(measurementData, user) {
        if ((measurementData.did === 0 || measurementData.did) && measurementData.tms) {

          // Why is this param set to 0?

            if (isNaN(measurementData.did) || isNaN(measurementData.tms)) {
                console.log(`Invalid did or tms defined in MeasurementData payload`, measurementData);
                response(200);
                return;
            }

            // dynamo.query({
            //         TableName: TABLE_DEVICES,
            //         KeyConditionExpression: "deviceId = :deviceId",
            //         ExpressionAttributeValues: {
            //             ":deviceId": measurementData.did
            //         }
            //     })

            dynamo.getItem({
                  TableName: TABLE_DEVICES,
                  Key: {
                    deviceId: measurementData.did
                  }
                })

                .promise()
                .then(deviceItem => {
                    // if (deviceItem.Items.length === 0) {
                    //     console.log(`No device with id ${measurementData.did}`, measurementData);
                    //     response(200);
                    //     return;
                    // }

                    if (!deviceItem.Item) {

                      console.log(`No device with id ${measurementData.did}`, measurementData);
                      response(200);
                      return;

                      // this may return a 200 because it will throw an error when this is parsed (automatically). if not: throw 204 or 404

                    }

                    let device = deviceItem.Item;
                    let assignedAirVent = device.airVentAssignments.find(assignment => isAssignedToDevice(assignment.fromTimestamp, assignment.toTimestamp, measurementData.tms));
                    if (!assignedAirVent) {
                        console.log(`No active AirVent assigned to device ${device.deviceId}`, measurementData);
                        response(200);
                        return;
                    }

                    // return dynamo.scan({
                    //         TableName: TABLE_BUILDINGS,
                    //         FilterExpression: "userId  = :userId",
                    //         ExpressionAttributeValues: {
                    //             ":userId": assignedAirVent.userId
                    //         }
                    //     })

                    return dynamo.getItem({
                      TableName: TABLE_BUILDINGS,
                      Key: {
                        id: user.buildingId[0],
                        userId: user.id
                        // write case for multiple buildings here
                      }
                    })
                        .promise()
                        .then(buildingItem => {
                            // if (buildingItem.Items.length !== 1) {
                            //     console.log(`No Building for User ${user.id}`);
                            //     response(200);
                            //     return;
                            // }

                            if (!buildingItem.Item) {
                                console.log(`No Building for User ${user.id}`);
                                response(200);
                                return;
                            }

                            let building = buildingItem.Item;

                            let airVent = building.airVents.find(airVent => airVent.id === assignedAirVent.airVentId);
                            if (!airVent) {
                                console.log(`No AirVent with id ${assignedAirVent.airVentId} assigned to User's building ${building.id}`, measurementData);
                                response(200);
                                return;
                            }

                            let activeAirFilter = airVent.airFilters.find(airFilter => isAssignedToDevice(airFilter.fromTimestamp, airFilter.toTimestamp, measurementData.tms));
                            if (!activeAirFilter) {
                                console.log(`No active AirFilter in AirVent with id ${airVent.id} for Building ${building.id}`, measurementData);
                                response(200);
                                return;
                            }

                            measurementData.airFilterId = activeAirFilter.id;
                            dynamo.putItem({
                                TableName: TABLE_MEASUREMENTS,
                                Item: measurementData
                            }).promise().then(response(200)).catch(err => {
                                console.log(`Error while persisting MeasurementData in database`, err);
                                response(500);
                            });

                        })
                        .catch(err => {
                            console.log(`Error while querying Building for User ${user.id}`, err);
                            response(500);
                        });
                })
                .catch(err => {
                    console.log(`Error while querying Device from database for deviceId ${measurementData.did}`, err);
                    response(500);
                });
        }
        else {
            console.log(`Measurement payload without tms or did`, measurementData);
            response(200);
        }
    }

    function isAssignedToDevice (fromTimestamp, toTimestamp, measurementTimestamp) {
        return fromTimestamp < measurementTimestamp && (toTimestamp === undefined || toTimestamp > measurementTimestamp);
    }

    function airFilterInBuilding(building, airFilterId) {
        for (let i = 0; i < building.airVents.length; ++i) {
            let airVent = building.airVents[i];
            if (airVent.airFilters) {
                if (airVent.airFilters.find(airFilter => airFilter.id === airFilterId)) {
                    return true;
                }
            }
        }
        return false;
    }

    function mapMeasurementDataToDTO(measurementData) {
        return {
            did: measurementData.did,
            fwv: measurementData.fwv,
            sig: measurementData.sig,
            bat: measurementData.bat,
            tms: measurementData.tms,
            per: measurementData.per,
            ctr: measurementData.ctr,
            bmv: measurementData.bmv,
            dat: measurementData.dat
        };
    }

};
