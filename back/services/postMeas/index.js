
const Core = require('./core');

const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var TABLE = process.env.TABLE;
var SUFFIX = process.env.SUFFIX;

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {

    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });

  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'POST') {
    response(405, 'Http method not allowed');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      let json = parseJSONArray(event.body, 'did');
      if (!Array.isArray(json)) {
        response(403, json);
      } else {
        let dIds = json.map(j => j.did);
        validateOwnsDevice(dIds, admin, org)
          .then(ownedDev => {
            json = assignFilters(json, ownedDev);
            persistMeasurements(json);
          }, ownerErr => {
            response(400, ownerErr);
          });
      }
    }, coreErr=> {
      response(coreErr);
    });

  function validateOwnsDevice(dIds, admin, org){

    let buildings = org.buildings;

    return new Promise((resolve, reject) => {
      Promise.all(buildings.map(b => {
        return new Promise(resolve => {
          Dynamo.getItem({
            TableName: 'Buildings-matt',
            Key: {
              id: b
            }
          }, (err, data) => {
            if (err || !data.Item || !data.Item.id || !data.Item.airVents) {
              resolve(null);
            } else {
              resolve(data.Item);
            }
          })
        });
      })).then(bldgs => {
        bldgs = bldgs.filter(b => b !== null);
        let devices = bldgs.map(b => b.airVents.map(v => {return {d: v.device, f: getCurrentFilter(v)}}));

        devices = devices.reduce((a, i) => {
          if (i.length > 0) {
            i.forEach(d => {
              if (d.f !== null) {
                a.push(d);
              }
            });
          }
          return a;
        });
        if (devices.length < 1) {
          findRecursive(dIds)
            .then(validDevs => {
              completePersistMeasurements(validDevs)
                .then(devs => {
                  resolve(devs);
                }, recursiveErr => {
                  reject(recursiveErr)
                })
            });
        }
        else {
          completePersistMeasurements(devices)
            .then(devs => {
                resolve(devs);
              }, recursiveErr => {
                reject(recursiveErr)
            });
        }
      }, ownerErr => {
        response(ownerErr.statusCode ? ownerErr.statusCode : 400, ownerErr.message ? ownerErr.message : 'Error parsing ownership of devices');
      });
    });

  }

  function completePersistMeasurements(devices){
    return new Promise((resolve, reject) => {
      let ids = devices.map(d => d.d);
      resolve(devices);
      if (dIds.every(deviceExists)) {
        resolve(devices);
      } else {
        reject(`Some or all devices that have emitted these measurements do not belong to organization ${org.name}`);
      }
      function deviceExists(d){
        return ids.indexOf(d) > -1;
      }
    })
  }

  function findRecursive(dids){
    return new Promise((resolve, reject) => {
      function scanBuildings(dids, devs, lastKey){
        let params = {
          TableName: 'Buildings-matt',
          Limit: 1
        };
        if (lastKey) {
          params.ExclusiveStartKey = lastKey
        }
        Dynamo.scan(params, (err, data) => {
          let foundVents = data.Items[0].airVents.filter(v => dids.includes(v.device));
          if (foundVents.length > 0) {
            foundVents = foundVents.map(f => {return {d: v.device, f: getCurrentFilter(f)}});
            console.log(foundVents);
            devs.concat(foundVents);
          }
          if (devs.length === dids.length) {
            resolve(devs);
          } else if (data.LastEvaluatedKey) {
            return findRecursive(dids, devs, data.LastEvaluatedKey);
          } else {
            response(400, 'One or more requested device was not found in DB');
          }
        })
      }
    });
  }

  function getCurrentFilter(vent){

    if (!vent.airFilters) {
      return null;
    }
    let currentFilter = vent.airFilters.find(f => !f.toTimestamp);
    if (currentFilter === undefined) {
      return null;
    } else {
      return currentFilter.id;
    }

  }

  function parseJSONArray(items){
    let arr;
    if (Array.isArray(items)) {
      arr = items;
    } else if (Array.isArray(JSON.parse(items))) {
      arr = JSON.parse(items);
    }
    try {
      if (arr.every(hasProp)) {
        return arr;
      } else {
        return 'At least one measurement does not contain a valid deviceId'
      }
    } catch(err) {
      return 'Request body must be in valid JSON format';
    }

    function hasProp(a){
      return a.did;
    }

  }

  function assignFilters(msmts, devices){
    msmts.forEach(m => {
      let dev = devices.find(d => d.d === m.did);
      console.log(dev);
      m.airFilterId = dev.f;
    });

    return msmts;

  }

  function persistMeasurements(msmts){
    Promise.all(msmts.map(m => {
      Dynamo.putItem({
        TableName: 'Measurements-matt',
        Item: m
      }, (err, data) => {
        if (err) {
          console.log({ERROR: `Error posting measurement for device ${m.did}`});
        } else {
          console.log(`Successfully posted measurement for device ${m.did}`);
        }
      })
    })).then(res => {
      response(200);
    }).catch(postErr => {
      response(postErr.statusCode ? postErr.statusCode : 403, postErr.message ? postErr.message : 'Error posting measurements');
    });
  }

}
