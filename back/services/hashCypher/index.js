
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const bcrypt = require('bcrypt');
const Cognito = new AWS.CognitoIdentityServiceProvider();

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': process.env.ORIGIN || '*'
      }
    });
  }
  let tkn;
  if (JSON.parse(event.body).token) {
    tkn = JSON.parse(event.body).token;
  } else if (event.body.token) {
    tkn = event.body.token;
  } else {
    response(403, 'Token must be present in response body');
  }

  const saltRounds = 10;
  let salt = bcrypt.genSaltSync(saltRounds);
  let hash = bcrypt.hashSync(tkn, salt);
  console.log(hash);
}
