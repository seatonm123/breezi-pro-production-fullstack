
const Index = require('./index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMiLCJldmVudF9pZCI6IjE4NjVlOWU1LTdlZmEtNDFhMi05OTQxLTI4ZDQ1ZTQ5NTY4ZSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzIyMjQ2MzYsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3MjIyODIzNiwiaWF0IjoxNTcyMjI0NjM2LCJqdGkiOiJhMjY3NzE3MC1kNzgxLTQ1ZDgtYjM3Zi0xYTJiYjAyYWY4YzgiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMifQ.V8qUwN8i5bu0FVbPZRHGh9FFnZsEZMvLDjRcAzmEa-3ltERstZvFxHxckMeq6MmvatcFgylFlGjjBe3SPMoV4HxUydAXoZWvGFWl6mqFta3CzS-hONpzP0LhmNW22WgYmf9EHnL0F6ka2r0qsbxEBuyoBP9dOZvREhA_Fb2Ld2scHDxfjmr5ESLPM3XiBsk61HIgw_p_x0RST1V3hD2mX38LqDtZAHiTGQ-qcGq1PHTu5qOd2lHmDbZw7huXSlbwLNvRs5uLulS6hhWdiimTxPwd8I2yBN-y1s_GEG_FWFav7Bqry5Dp8-mkgJU6720be8QTqjsjsDIB0cgQFIllFw`;

const TestEvent = {
  httpMethod: 'POST',
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    // buildingId: '9162edf2-11da-4913-823e-ba7df8eeebeb',
    // id: 'e8043176-bc51-497f-b454-201832e46ced'
    // hvacName: 'Reception hall',
    // fBrand: 'BreezAirre',
    // size: 'REEEL BIGGG',
    // fType: 'box'
  })
}

Index.handler(TestEvent, {}, (err, res) => {
  err ? console.error({ERROR: err}) : console.log(res);
})
