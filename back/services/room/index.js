
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let BUILDINGS = `Buildings${process.env.SUFFIX ? process.env.SUFFIX : '-matt'}`;
let DEVICES = `Devices${process.env.SUFFIX ? process.env.SUFFIX : '-matt'}`;
let USERS = `Users${process.env.SUFFIX ? process.env.SUFFIX : '-matt'}`;

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    })
  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be included in request Authorization header');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      const admin = details.user;
      const org = details.org;
      handleHttpRequest(event, admin, org);
    }, coreErr => {
      response(coreErr.code, coreErr.msg);
    });

  function handleHttpRequest(event, admin, org){

    let body;
    switch (event.httpMethod) {
      case 'POST':
        handlePost(event, admin, org);
      break;
      case 'PUT':
        body = Core.parseFromJson(event.body, 'id');
        if (!body.id) {
          response(403, body);
        }
        if (!body.buildingId) {
          response(403, `Building id is required to UPDATE airVent ${body.airVentId}`);
        }
        handlePut(org, body);
      break;
      case 'DELETE':
        body = Core.parseFromJson(event.body, 'id');
        console.log({BODY: body});
        if (!body.id) {
          response(403, body);
        }
        if (!body.buildingId) {
          response(403, `Building id is required to DELETE airVent ${body.id}`);
        }
        validateBody(body, org)
          .then(validBldg => {
            handleDelete(body.id, validBldg)
          }, err => {
            response(401, err);
          });
      break;
      default:
        response(405, 'Http method not allowed');
    }

  }



    /*

      All non-required props default to 'notSpecified'

      TEMPLATE
        @ac : bool (required)
        @aq : bool (required)
        REMEMBER: Schema will be dictated by these bools on frontend
        IF (ac === true) {
          @configuration : str (required)
          @manufacturer : str
          @modelNo : str
          @size : str (required)
          @fModel : str
          @fType : str (required)
        }

        @hvacName : str (required) {room name}
        @buildingId : str (required)
        @floorIndex : num (required)
        @floorName : str

        ADDED AUTOMATICALLY

        @airFilters : arr

    */

  function handlePost(event, admin, org){
    validatePost(event, admin, org)
      .then(valid => {
        if (valid.code) {
          response(validate.code, validate.msg);
        }
        let bldg = valid.bldg;
        let room = valid.room;
        validateDevice(bldg, room)
          .then(dev => {
            let thisFloor = bldg.floors.active.find(f => f.index === room.floorIndex);
            room.id = uuid();
            let now = Math.floor(Date.now()/1000);
            if (thisFloor === undefined) {
              thisFloor = {
                id: uuid(),
                index: room.floorIndex,
                name: room.floorName ? room.floorName : `Floor ${room.floorIndex.toString()}`,
                rooms: [room.id]
              }
              bldg.floors.active.push(thisFloor);
            } else {
              thisFloor.rooms.push(room.id);
            }
            let hwv = room.hardwareVersion;
            delete room.buildingId;
            delete room.hardwareVersion;
            room.active = true;
            room.airFilters = [
              {
                id: uuid(),
                fromTimestamp: now
              }
            ];
            if (room.floorName) delete room.floorName;
            bldg.airVents.push(room);
            updateBldg(bldg, org, room, hwv, now);
          }, devErr => {
            response(devErr.code, devErr.msg);
          });
      }).catch(err => {
        response(err.code, err.msg);
      })
  }

  function handlePut(){

  }

  async function validateBody(b, o){

    let bId = b.buildingId;
    let ventId = b.id;
    let bldg = await Dynamo.getItem({
      TableName: BUILDINGS,
      Key: {
        id: bId
      }
    }).promise();
    if (!bldg.Item || !bldg.Item.id) {
      return Promise.reject(`Building with id ${bId} not found`);
    }
    bldg = bldg.Item;
    if (bldg.org !== o.name) {
      return Promise.reject(`Building ${bldg.name} does not belong to org ${o.name}`);
    }
    let validVent = bldg.airVents.find(v => v.id === ventId);
    if (validVent === undefined) {
      return Promise.reject(`AirVent ${ventId} not found in building ${bldg.name}`);
    }
    return Promise.resolve(bldg);

  }

  function handleDelete(vId, bldg){

    let thisVent = bldg.airVents.find(v => v.id === vId);
    bldg.airVents.splice(bldg.airVents.indexOf(thisVent), 1);
    bldg.floors.active.forEach(f => {
      if (f.rooms.includes(vId)) {
        f.rooms.splice(f.rooms.indexOf(f), 1);
        if (f.rooms.length === 0) {
          bldg.floors.active.splice(bldg.floors.active.indexOf(f), 1);
        }
      }
    });
    console.log({BEFORE: {VENTS: bldg.airVents.length, FLOORS: bldg.floors.active.length}});
    Dynamo.updateItem({
      TableName: BUILDINGS,
      Key: {
        id: bldg.id
      },
      UpdateExpression: 'SET airVents = :vents, floors = :floors',
      ExpressionAttributeValues: {
        ':vents': bldg.airVents,
        ':floors': bldg.floors
      }
    }, (err, data) => {
      if (err) {
        console.log(err);
        response(err.statusCode ? err.statusCode : 400, err.message ? err.message : `Could not update building ${bldg.name}`);
      } else {
        console.log(`Removed airVent ${vId} from building ${bldg.name}. Deleting device ${thisVent.device}`);
        deleteDevice(thisVent.device);
      }
    });

  }

  function deleteDevice(dev){
    Dynamo.deleteItem({
      TableName: DEVICES,
      Key: {
        deviceId: dev
      }
    }, (err, data) => {
      if (err) {
        response(err.statusCode ? err.StatusCode : 400, err.message ? err.message : `Could not delete device ${dev}`);
      } else {
        response(`Device ${dev} deleted. Successfully removed room`);
      }
    });
  }

  function validatePost(event, admin, org){

    return new Promise((resolve, reject) => {
      let body = validateStructure(event.body || null);
      if (body.code) {
        reject(body);
      }
      validateBldgPermissions(body.buildingId, org)
        .then(bldg => {
          resolve({bldg: bldg, room: body});
        }).catch(vErr => {
          reject(vErr);
        });
    });

  }

  function validateStructure(body){
    if (body === null) {
      return {code: 403, msg: 'New room object must be present in request body'};
    }
    let notJson = Core.parseFromJson(body, 'hvacName');
    if (!notJson.hvacName) {
      return {code: 403, msg: 'New room object must be in valid JSON format'};
    }
    console.log(body);
    let schema = ['aq', 'hvacName', 'buildingId', 'floorIndex', 'device', 'hardwareVersion'];
    if (notJson.ac && notJson.ac === true) {
      schema = schema.concat(['configuration', 'size', 'fType'])
    }
    if (notJson.ac === false && notJson.aq === false) {
      return {code: 403, msg: 'Measurement schema must be specified'};
    }
    if (!schema.every(hasProp)) {
      return {code: 403, msg: `Request body must include ${schema.join(', ')} properties`};
    }
    function hasProp(prop){
      return notJson[prop];
    }
    if (isNaN(notJson.device)) {
      return {code: 403, msg: 'Device id must be a number'};
    }
    return notJson
  }

  function validateBldgPermissions(bId, org){
    console.log(bId);
    return new Promise((resolve, reject) => {
      Dynamo.getItem({
        TableName: BUILDINGS,
        Key: {
          id: bId
        }
      }).promise()
        .then(building => {
          if (!building.Item || !building.Item.id) {
            reject({code: 400, msg: `Building ${bId} not found`});
          }
          if (building.Item.org !== org.name) {
            reject({code: 401, msg: `Building ${bId} belongs to a different user`});
          }
          resolve(building.Item);
        });
    });
  }

  function validateDevice(bldg, room){
    return new Promise((resolve, reject) => {
      Dynamo.getItem({
        TableName: DEVICES,
        Key: {
          deviceId: room.device
        }
      }, (err, data) => {
        if (err) {
          reject({code: 400, msg: `Error validating device ${room.device}`})
        } else {
          if (data.Item && data.Item.deviceId) {
            let activeVent = bldg.airVents.find(v => v.device === data.Item.deviceId);
            if (activeVent !== undefined && activeVent.active === true) {
              reject({code: 400, msg: `Device ${room.device} is already registered to vent ${activeVent.id}`})
            }
          }
        }
        resolve(true);
      });
    });
  }

  function updateBldg(bldg, uId, room, hwv, now) {
    Dynamo.updateItem({
      TableName: BUILDINGS,
      Key: {
        id: bldg.id
      },
      UpdateExpression: 'set airVents = :vents, floors = :floors',
      ExpressionAttributeValues: {
        ':vents': bldg.airVents,
        ':floors': bldg.floors
      }
    }, (err, data) => {
      if (err) {
        response(err.statusCode ? err.statusCode : 400, err.message ? err.message : err);
      } else {
        persistNewDevice(room, uId, hwv, now);
      }
    })
  }

  function persistNewDevice(room, org, hwv, now){
    Dynamo.getItem({
      TableName: USERS,
      Key: {
        email: org.masterAdmin
      }
    }).promise()
      .then(master => {
        if (!master.Item || !master.Item.email) {
          response(400, 'Master admin data not found');
        } else {
          master = master.Item;
        }
        let device = {
          deviceId: room.device,
          hardwareVersion: hwv,
          id: uuid(),
          userId: master.id,
          airVentAssignments: [
            {
              airVentId: room.id,
              userId: master.id,
              fromTimestamp: now
            }
          ]
        }
        Dynamo.putItem({
          TableName: DEVICES,
          Item: device
        }, (err, data) => {
          if (err) {
            response(err.statusCode ? err.statusCode : 400, err.message ? err.message : err);
          } else {
            response(200, room);
          }
        })
      });
  }

  function handlePut(org, room){
    let mutable = validateUpdate(room);
    getBldg(room.buildingId, org)
      .then(bldg => {
        let thisVent = bldg.airVents.find(v => v.id === room.id);
        if (thisVent === undefined) {
          response(400, `Room ${room.id} not found`);
        }
        Object.keys(mutable).forEach(k => {
          if (thisVent[k] !== mutable[k]) {
            thisVent[k] = mutable[k];
          }
        });
        let newVents = [];
        bldg.airVents.forEach(v => {
          if (v.id !== thisVent.id) {
            newVents.push(v);
          }
        });
        newVents.push(thisVent);
        bldg.airVents = newVents;
        persistBldgUpdate(bldg);
      }, err => {
        response(400, err);
      });
  }

  function validateUpdate(room){
    let validParams = [
      'hvacName',
      'altConfig',
      'altSize',
      'fBrand',
      'fModel',
      'fType',
      'hardwareVersion'
    ];
    let newRoom = {};
    Object.keys(room).forEach(k => {
      if (validParams.indexOf(k) > -1) {
        newRoom[k] = room[k];
      }
    });
    return newRoom;
  }

  function getBldg(bId, org){
    return new Promise((resolve, reject) => {
      if (org.buildings.indexOf(bId) < 0) {
        reject (`Building ${bId} does not belong to organization ${org.name}`);
      }
      Dynamo.getItem({
        TableName: 'Buildings-matt',
        Key: {
          id: bId
        }
      }, (err, data) => {
        if (err) {
          reject(err.message);
        }
        if (!data.Item) {
          reject(`Building ${bId} not found`);
        }
        resolve(data.Item);
      })
    });
  }

  function persistBldgUpdate(bldg){
    Dynamo.updateItem({
      TableName: 'Buildings-matt',
      Key: {
        id: bldg.id
      },
      UpdateExpression: 'set airVents = :vents',
      ExpressionAttributeValues: {
        ':vents': bldg.airVents
      }
    }, (err, data) => {
      if (err) {
        response(400, `Could not persist update to building ${bldg.name}`);
      }
      response(200, `Building ${bldg.name} updated with new vent data`);
    })
  }

};
