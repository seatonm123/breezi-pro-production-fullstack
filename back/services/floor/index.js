
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let BUILDINGS = `Buildings-${process.env.SUFFIX || 'matt'}`;

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  };

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let org = details.org;
      let admin = details.user;
      let body = Core.parseFromJson(event.body, 'bId');
      if (!body.bId) {
        response(403, 'Building object must be included in request body');
      }
      validateFloor(body, org)
        .then(valid => {
          handleHttpMethod(event, valid, org, admin);
        }, vErr => {
          response(vErr);
        });
    });

  function validateFloor(body, org){
    return new Promise((resolve, reject) => {
      Dynamo.getItem({
        TableName: BUILDINGS,
        Key: {
          id: body.bId
        }
      }, (err, data) => {
        if (err) {
          reject({code: 400, msg: 'Error finding requested building'});
        } else if (!data.Item || !data.Item.id) {
          reject({code: 400, msg: 'Requested building not found'});
        } else {
          delete body.bId;
          if (!body.name || !body.index) {
            reject({code: 403, msg: 'Floor object must contain name and index properties'});
          }
          if (data.Item.org !== org.name) {
            reject({code: 401, msg: 'Requested building is owned by a different organization'});
          }
          resolve({floor: body, building: data.Item});
        }
      });
    });
  }

  function handleHttpMethod(event, valid, org, admin){

    switch (event.httpMethod) {
      case 'POST':
        handlePost(valid, org, admin);
      break;
      case 'PUT':
        handlePut(valid, org, admin);
      break;
      case 'DELETE':
        handleDelete(valid, org, admin);
      break;
      default:
        response(405, 'Http method not allowed');
    }

  }

  function handlePost(valid, org, admin){
    let bldg = valid.building;
    let floor = valid.floor;
    if (!validateIndex(bldg, floor)) {
      response(403, 'Floor at requested index is already registered or is out of range');
    } else {
      floor.id = uuid();
      floor.rooms = [];
      bldg.floors.active.push(floor);
      Dynamo.updateItem({
        TableName: BUILDINGS,
        Key: {
          id: bldg.id
        },
        UpdateExpression: 'set floors = :floors',
        ExpressionAttributeValues: {
          ':floors': bldg.floors
        }
      }, (err, data) => {
        if (err) {
          response(400, `Building ${bldg.id} could not be updated`);
        } else {
          response(200, `Floor ${floor.id} has been added to building ${bldg.id}`);
        }
      });
    }
  }

  function handlePut(valid, org, admin){
    let bldg = valid.building;
    let floor = valid.floor;
    if (!floor.id) {
      response(403, 'Floor id to be updated must be included in request body');
    } else {
      let floorToUpdate = bldg.floors.active.find(f => f.id === floor.id);
      if (floorToUpdate === undefined) {
        response(403, 'Requested floor not found');
      } else {
        bldg.floors.active[bldg.floors.active.indexOf(floorToUpdate)] = floor;
        Dynamo.updateItem({
          TableName: BUILDINGS,
          Key: {
            id: bldg.id
          },
          UpdateExpression: 'set floors = :floors',
          ExpressionAttributeValues: {
            ':floors': bldg.floors
          }
        }, (err, data) => {
          if (err) {
            response(err.code ? err.code : 400, err.message ? err.message : err);
          } else {
            response(200, `Floor ${floor.id} has been updated`);
          }
        });
      }
    }
  }

  function handleDelete(valid, org, admin) {
    let bldg = valid.building;
    let floor = valid.floor;
    if (!floor.id) {
      response(403, 'Floor id to be updated must be included in request body');
    } else {
      let floorToDelete = bldg.floors.active.find(f => f.id === floor.id);
      if (floorToDelete === undefined) {
        response(403, 'Requested floor not found');
      }
      bldg.floors.active = bldg.floors.active.filter(f => f.id !== floor.id);
      Dynamo.updateItem({
        TableName: BUILDINGS,
        Key: {
          id: bldg.id
        },
        UpdateExpression: 'set floors = :floors',
        ExpressionAttributeValues: {
          ':floors': bldg.floors
        }
      }, (err, data) => {
        if (err) {
          response(err.code ? err.code : 400, err.message ? err.message : err);
        } else {
          response(200, `Floor ${floor.id} has been successfully deleted`);
        }
      });
    }
  }

  function validateIndex(bldg, floor){
    if (bldg.floors.active.find(f => f.index === floor.index) !== undefined) {
      return false
    }
    let low = 0 - bldg.floors.belowGround;
    let high = bldg.floors.aboveGround;
    return (floor.index >= low && floor.index <= high)
      ? true
      : false;
  }

}
