const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const Cognito = require('amazon-cognito-identity-js-node');
const cognito = new AWS.CognitoIdentityServiceProvider();

const UserPoolData = {
  UserPoolId: process.env.USER_POOL_ID,
  ClientId: process.env.CLIENT_ID
};

AWS.CognitoIdentityServiceProvider.AuthenticationDetails = Cognito.AuthenticationDetails;
AWS.CognitoIdentityServiceProvider.CognitoUserPool = Cognito.CognitoUserPool;
AWS.CognitoIdentityServiceProvider.CognitoUser = Cognito.CognitoUser;

exports.validatePartner = function(req){
  return new Promise((resolve, reject)=>{
    if (req === null) {
      reject({code: 422, msg: 'Request body must be valid JSON: {"email": "{yourEmail}", "password": "{yourPassword}"}'});
    } else {
      let userPool = new AWS.CognitoIdentityServiceProvider.CognitoUserPool(UserPoolData);
      let username = req.email;
      let password = req.password;
      let authenticationData = {
        Username: username,
        Password: password
      };
      let authenticationDetails = new AWS.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
      let userData = {
        Username: username,
        Pool: userPool
      };
      let cognitoUser = new AWS.CognitoIdentityServiceProvider.CognitoUser(userData);
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function(result){
          let tkn = result.getAccessToken().getJwtToken().toString();
          cognito.getUser({AccessToken: tkn}, (err, partner)=>{
            err ?
              reject({code: 403, msg: 'No partner found with given credentials'}) :
              resolve({partner: partner.Username, email: partner.UserAttributes.find(u=>u.Name === 'email').Value,  data: tkn});
          });
        },
        onFailure: function(result){
          reject({code: 403, msg: 'User not authorized'});
        }
      });
    }
  });
};
