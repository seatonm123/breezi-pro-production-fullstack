exports.validateRequestBody = function(body){
  try {
    if (!body || body === null || body === undefined) {
      return null;
    } else {
      return typeof(body) === 'string' ?
        JSON.parse(body) : body;
    }
  }
  catch(err) {
    return null;
  }
};
