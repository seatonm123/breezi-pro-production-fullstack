const Validate = require('./validate');
const Auth = require('./cognito');

exports.handler = function(event, context, cb){
  const response = function(code, msg){
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  if (event.httpMethod === 'POST') {
    let body = Validate.validateRequestBody(event.body);
    Auth.validatePartner(body)
      .then(tkn=>{
        response(200, tkn.data);

      }, tknErr=>{
        response(tknErr.code, tknErr.msg);
      });
  } else {
    response(405, 'HTTP method not allowed');
  }
};
