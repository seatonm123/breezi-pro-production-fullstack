'use strict';

const doc = require('dynamodb-doc');
const Common = require('brebe-common');
const dynamo = new doc.DynamoDB();
const common = new Common();
const USERS_TABLE_NAME = `Users${process.env.ENVIRONMENT_SUFFIX ? process.env.ENVIRONMENT_SUFFIX : ''}`;
const BUILDINGS_TABLE_NAME = `Buildings${process.env.ENVIRONMENT_SUFFIX ? process.env.ENVIRONMENT_SUFFIX : ''}`;
const ORG_TABLE_NAME = `Organizations${process.env.ENVIRONMENT_SUFFIX}`;

exports.handler = (event, context, callback) => {
    let email = event.request.userAttributes.email;

    if (email) {
        switch (event.triggerSource) {
            case 'CustomMessage_SignUp':
                preSignUp(email, event, callback);
                break;
            case 'CustomMessage_AdminCreateUser':
                preSignUp(email, event, callback);
                break;
            case 'PostConfirmation_ConfirmSignUp':
                let isExternalProvider = event.request.userAttributes['cognito:user_status'] === 'EXTERNAL_PROVIDER';
                if (isExternalProvider) {
                    confirmUpExternalProvider(email, event, callback);
                }
                break;
            default:
                console.log(`Unsupported event type ${event.triggerSource}`);
                callback(null, event);
        }
    }

    callback(null, event);
};

function preSignUp(email, event, callback) {
    let lastName = event.request.userAttributes.family_name;
    let firstName = event.request.userAttributes.given_name;
    let phoneNumber = event.request.userAttributes.phone_number;
    let masterAdmin = event.request.userAttributes['custom:masterAdmin'] === 'true' ? true : false;
    let admin = event.request.userAttributes['custom:admin'] === 'true' ? true : false;
    let orgId = event.request.userAttributes['custom:orgId'];

    getUserByEmail(email)
      .then(user => {
        user.firstName = firstName;
        user.lastName = lastName;
        if (phoneNumber) user.phoneNumber = phoneNumber;
        if (!user) {
          if (masterAdmin === true) {
            persistMasterAdmin(user, orgId);
          }
          if (admin === true) {
            persistAdmin(user, orgId);
          }
          persistTypicalUser(user);
        } else {
          user.isAdmin = (admin && admin === true) ? true : false;
          return persistUser(user);
        }
        // remember: all must return promises
      })
      .then(() => {
          callback(null, event);
      })
      .catch(error => {
          console.log("Exception while processing user-persist request", error);
          callback(null, error);
      });

    function persistMasterAdmin(user){
      user.orgId = orgId;
      user.buildings = [common.generateUUID()];
      // user.isAdmin = true;
      user.isMasterAdmin = true;
      persistUser(user).then(() => putBuilding(user).then(() => putOrganization(user)));
    }

    function persistAdmin(user, orgId){
      user.orgId = orgId,
      user.buildings = [],
      user.isAdmin = true;
      persistUser(user);
    }

    function persistUser(user){
      return dynamo.putItem({
        TableName: USERS_TABLE_NAME + process.env.ENVIRONMENT_SUFFIX,
        Item: user
      }).promise();
    }

    function putBuilding(user){
      let building = {
        id: common.generateUUID(),
        orgId: user.orgId,
        admins: [user.id],
        users: [],
        userId: user.id,
      }
      return dynamo.putItem({
        TableName: BUILDINGS_TABLE_NAME + process.env.ENVIRONMENT_SUFFIX,
        Item: building
      }).promise();
    }

    function putOrganization(user){
      let org = {
        id: user.orgId,
        buildings: user.buildings,
        masterAdmin: user.id,
        admins: [user.id],
        users: []
      }
    }

//     getUserByEmail(email)
//         .then(item => {
//             let user = item.Item;
//             if (!user) {
//                 user = {
//                     "id": common.generateUUID(),
//                     "email": email,
//                     "buildings": [common.generateUUID()],
//                     "orgId": common.generateUUID(),
//                     "isMasterAdmin": true,
//                     "isAdmin": true
//                 };
//                 if(firstName && firstName.length > 0) {
//                     user.firstName = firstName;
//                 }
//
//                 if(lastName && lastName.length > 0) {
//                     user.lastName = lastName;
//                 }
//
//                 if(phoneNumber && phoneNumber.length > 0) {
//                     user.phoneNumber = phoneNumber;
//                 }
//                 return persistUser(user).then(() => persistBuilding(user).then(() => persistOrganization(user)));
//             }
//             else {
//                 user.firstName = firstName;
//                 user.lastName = lastName;
//                 if (phoneNumber) user.phoneNumber = phoneNumber;
//
//                 return persistUser(user);
//             }
//         })
//         .then(() => {
//             callback(null, event);
//         })
//         .catch(error => {
//             console.log("Exception while processing user-persist request", error);
//             callback(null, error);
//         });
// }

function confirmUpExternalProvider(email, event, callback) {
    let amazonAccountId;
    let identities = JSON.parse(event.request.userAttributes.identities);
    if (identities && identities.length > 0 && identities[0].providerType === "LoginWithAmazon") {
        amazonAccountId = identities[0].userId;
    }

    getUserByEmail(email)
        .then(item => {
            let user = item.Item;
            if (user) {
                user.amazonAccountId = amazonAccountId;
                return persistUser(user);
            }
            else {
                let user = {
                    "id": common.generateUUID(),
                    "email": email,
                    "amazonAccountId": amazonAccountId
                };
                return persistUser(user).then(persistBuilding(user));
            }
        })
        .then(() => {
            callback(null, event);
        })
        .catch(error => {
            console.log("Exception while processing user-persist request", error);
            callback(null, event);
        });
}

function getUserByEmail(email) {
    return dynamo.getItem({
            TableName: USERS_TABLE_NAME,
            Key: {
                "email": email
            }
        })
        .promise();
}

// function persistUser(user, callback) {
//     return dynamo.putItem({
//         TableName: USERS_TABLE_NAME,
//         Item: user
//     }).promise();
// }
//
// function persistBuilding(user) {
//     return dynamo.putItem({
//         TableName: BUILDINGS_TABLE_NAME,
//         Item: {
//             id: user.buildings[0],
//             userId: user.id
//         }
//     }).promise();
// }
//
// function persistOrganization(user){
//   let organization = {
//     id: user.orgId,
//     masterAdmin: user.id,
//     joined: Math.floor(Date.now()/1000),
//     buildings: user.buildings
//   };
//   return dynamo.putItem({
//     TableName: ORG_TABLE_NAME,
//     Item: organization
//   }).promise();
}
