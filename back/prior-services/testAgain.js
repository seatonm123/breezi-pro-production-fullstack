
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const uuid = require('uuid');

function cognitoCreateUser(){

  const params = {
    UserPoolId: process.env.USER_POOL_ID || 'eu-west-1_HJnusXcgZ',
    Username: 'matt.r.seaton@breezi.io',
    DesiredDeliveryMediums: [
      'EMAIL'
    ],
    ForceAliasCreation: false,
    UserAttributes: [
      {
        Name: 'email',
        Value: ''
      },
      {
        Name: 'custom:orgId',
        Value: uuid()
      },
      {
        Name: 'custom:masterAdmin',
        Value: 'true'
      },
      {
        Name: 'custom:admin',
        Value: 'false'
      }
    ],
    ValidationData: [
      {
        Name: 'email',
        Value: 'matt.r.seaton@breezi.io'
      }
    ]
  };
  return Cognito.adminCreateUser(params).promise();

}

cognitoCreateUser().then(() => console.log('worked'))
