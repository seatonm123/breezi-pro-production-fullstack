
const validator = require('email-validator');
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let SUFFIX = process.env.SUFFIX || '-matt';

module.exports = {

  validateAndGetUserData(tkn){

    return new Promise((resolve, reject) => {
      getCognitoDetails(tkn)
        .then(cUser => {
          getDynamoDetails(cUser)
            .then(dUser => {
              getOrgDetails(dUser)
                .then(details => {
                  if (!details.Item || !details.Item.id) {
                    reject({code: 400, msg: 'Organization not found in db'});
                  }
                  let org = details.Item;
                  resolve({user: dUser.Item, org: org});
                }, err => {
                  reject({code: 400, msg: err});
                })
            }, dErr => {
              reject({code: 400, msg: dErr});
            })
        }, cErr => {
          reject({code: 401, msg: cErr});
        })
    });

  }

}

function getCognitoDetails(tkn){
  return Cognito.getUser({AccessToken: tkn}).promise();
}

function getDynamoDetails(user){
  let email = user.UserAttributes.find(a => a.Name === 'email');
  if (email === undefined) {
    return Promise.reject('User not found in user pool');
  }
  email = email.Value;
  return Dynamo.getItem({
    TableName: 'Users' + SUFFIX,
    Key: {
      email: email
    }
  }).promise();
}

function getOrgDetails(user){
  if (!user.Item || !user.Item.email) {
    return Promise.reject('User not found in database');
  }
  return Dynamo.getItem({
    TableName: 'Organizations' + SUFFIX,
    Key: {
      id: user.Item.orgId
    }
  }).promise();
}

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzYzgyOGFiNy05OTgxLTRmZjItYjc4Mi0yY2M3NDEwMGZhMDIiLCJldmVudF9pZCI6ImZlN2YwOWZlLTExNTYtNGEwOS1iN2RjLWYxMzQ2NTFlMGNmMCIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NjU1NTkyNTMsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU2NTU2Mjg1MywiaWF0IjoxNTY1NTU5MjUzLCJqdGkiOiJjNjFjNzAwOS02YjdlLTRhM2UtYjRjZi1kNWZkYzhiYWI3YzEiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiIzYzgyOGFiNy05OTgxLTRmZjItYjc4Mi0yY2M3NDEwMGZhMDIifQ.BTPwY_wiG0sZ4eUUfY6BsjAA7w5OpR7cTNqqGbI3Yj0Xcic5N5BFVxSLqCw5I4-6dXyiuUbifMQHd48oVRrObvtOdyO-boPnJBezyherkzJGVNXQxPXXtN6g5Ws5Ov8CAOwibPHMFpKg2U_hFE6-izD-mF4w3S5Ks3riYZ7UpwxVRpotNwtkhyUacmNkXeG3J5GPncJ_p5mCgMoK4Vo2sKGN51E-iZO-rC77VjGAmnoT2Zu2-FfdTq911kR_xiflYP8pqCymIbklDoYoQoac_b9i3n2ylrThM5zzHkohSReG0YXAjsFA_KzzoLOU5_b4zZdl0QB-cMWpakPdyzuphg`;

module.exports.validateAndGetUserData(tkn)
  .then(details => {
    console.log(details);
  }, err => {
    console.log({ERROR: err});
  });
