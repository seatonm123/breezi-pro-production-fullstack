
const AddUser = require('./addUpdateUser/index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzYzgyOGFiNy05OTgxLTRmZjItYjc4Mi0yY2M3NDEwMGZhMDIiLCJldmVudF9pZCI6ImNjZjNmNmZjLTQ2ZjktNGJjYS1iMjE1LTI1ZmFjODg1ZDU1YyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NjU1NjQ3MDEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU2NTU2ODMwMSwiaWF0IjoxNTY1NTY0NzAxLCJqdGkiOiJmNTg5MTgyYS0zMGUxLTQ1NWQtYjY3Ni03ZjA5Zjc2M2NjZDciLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiIzYzgyOGFiNy05OTgxLTRmZjItYjc4Mi0yY2M3NDEwMGZhMDIifQ.SX831atgWRIQbGRfeStwhNtPz2JOA6QcHnc2RKHKJRSTPrTIwp7Zpc8Bydvdqok74nmrDOcjZSSWmkw8jX_PhgogrAyVzSwcjs5jDkXWSZZTq9KfZnYJ5U_-c7-LFduFD7YxZkzkU6v6cGAJHrdOfeAqNcg09Ab4SrESmCOcPQn2qV0UDRLZxpeHX05SU-b0HTezrC56tKO234214o1JZ2UEQWqw3dgj4erGymUdULt9RWn3jt6KVySnN5kajlOrGaLXKr058KO10lZ_MKP_nKtTJ1k1D0dahDGWvj_SCLwzSr8XxAZ22JHQ0fZaSzWxEjggi_QSVrU3Z6jNO-CXIQ`;

const TestEvent = {
  httpMethod: 'POST',
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  },
  // resource: '/users/{email}',
  // pathParameters: {
  //   'email': 'seatonm123@gmail.com'
  // },
  body: {
    "isAdmin": true,
    "email": "matt.r.seaton@gmail.com",
    "buildings": ['738027c5-761b-4984-b205-5b25ae9e1968'],
    "firstName": "Roscoe",
    "lastName": "Seaton",
    "jobTitle": "Lead Maintenance Personnel",
    "isMasterAdmin": false
  }
}

AddUser.handler(TestEvent, '', (err, res) => {
  err ? console.log(err) : console.log(JSON.stringify(res, null, 1));
});
