
const validator = require('email-validator');
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let SUFFIX = process.env.SUFFIX || '-matt';

module.exports = {

  validateAndGetUserData(tkn){

    return new Promise((resolve, reject) => {
      getCognitoDetails(tkn)
        .then(cUser => {
          getDynamoDetails(cUser)
            .then(dUser => {
              getOrgDetails(dUser)
                .then(details => {
                  if (!details.Item || !details.Item.id) {
                    reject({code: 400, msg: 'Organization not found in db'});
                  }
                  let org = details.Item;
                  resolve({user: dUser.Item, org: org});
                }, err => {
                  reject({code: 400, msg: err});
                })
            }, dErr => {
              reject({code: 400, msg: dErr});
            })
        }, cErr => {
          reject({code: 401, msg: cErr});
        })
    });

  }

}

function getCognitoDetails(tkn){
  return Cognito.getUser({AccessToken: tkn}).promise();
}

function getDynamoDetails(user){
  let email = user.UserAttributes.find(a => a.Name === 'email');
  if (email === undefined) {
    return Promise.reject('User not found in user pool');
  }
  email = email.Value;
  return Dynamo.getItem({
    TableName: 'Users' + SUFFIX,
    Key: {
      email: email
    }
  }).promise();
}

function getOrgDetails(user){
  if (!user.Item || !user.Item.email) {
    return Promise.reject('User not found in database');
  }
  return Dynamo.getItem({
    TableName: 'Organizations' + SUFFIX,
    Key: {
      id: user.Item.orgId
    }
  }).promise();
}
