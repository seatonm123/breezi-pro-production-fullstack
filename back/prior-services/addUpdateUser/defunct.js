
const validator = require('email-validator');
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const SUFFIX = process.env.SUFFIX || '-matt';

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  getUserDetails(event)
    .then(myDetails => {
      handleHttpReq(event, myDetails);
    }, err => {
      response(401, err);
    });


  async function getUserDetails(event){

    try {
      let cogUser = await Cognito.getUser({AccessToken: event.headers.Authorization}).promise();
      let email = cogUser.UserAttributes.find(att => att.Name === 'email').Value;
      let userItem = await Dynamo.getItem({
        TableName: 'Users' + SUFFIX,
        Key: {
          email: email
        }
      }).promise();
      user = userItem.Item;
      let org = await Dynamo.getItem({
        TableName: 'Organizations' + SUFFIX,
        Key: {
          id: user.orgId
        }
      }).promise();
      if (!org.Item || !org.Item.id) {
        throw(`No associated organization was found for user ${user.email}`);
      } else {
        return Promise.resolve({user: user, organization: org});
      }
    } catch(err) {
      return Promise.reject(err);
    }

  }


  function handleHttpReq(event, details){

    switch (event.httpMethod) {
      case 'GET':
        // wait on this until
        getUsers('');
      break;
      case 'POST':
        addNewUser(event.body, details)
      break;
      case 'PUT':
        return initUserUpdate(event, details);
      break;
      case 'DELETE':
        deleteUser(event, details);
      break;
      default:
        response(405, 'Http method not supported');
    }

  }


  function addNewUser(newUser, details){

    let user = newUser;
    if (!user.email) {
      response(400, 'User object must be valid JSON');
    }
    if (details.user.isAdmin) {
      if (user.isMasterAdmin) {
        response(401, 'Only one master admin is allowed per organization');
      }
      if (!validateUserProps(newUser)) {
        response(400, 'New user does not include all obligatory properties');
      }
      user.id = uuid();
      user.orgId = details.user.orgId;
      try {
        cognitoCreateUser(user, details.user)
          .then(dynamoCreateUser(user));
      } catch(err) {
        console.log(err)
      }
    } else {
      response(403, 'Only account administrators can add new users');
    }


  }


  function initUserUpdate(event, details){

    let user;
    if (event.resource === '/users/{email}' && event.pathParameters && event.pathParameters.email) {
      user = event.body;
      if (!details.user.isAdmin) {
        response(403, 'Only account administrators can update users');
      } else {
        try {
          let userToUpdate = event.resource.pathParameters.email;
          Dynamo.getItem({
            TableName: 'Users' + SUFFIX,
            Key: {
              email: userToUpdate
            }
          }).promise()
            .then(existingUser => {
              let updateDetails = validateUserUpdate(existingUser, event, details);
              persistUpdatedUser(updateDetails.existingUser, updateDetails.newUser);
            })
        } catch(err) {
          response(400, err);
        }
      }
    } else {
      user = consolidateUserVersions(event.body, details.user);
      Dynamo.putItem({
        TableName: 'Users' + SUFFIX,
        Item: user
      }, (err, data) => {
        if (err) {
          response(400, err);
        } else {
          response(200, 'User information has been successfully updated');
        }
      });
    }

  }


  function consolidateUserVersions(newUser, existingUser){

    if (newUser.buildings) {
      newUser.buildings.map(b => {
        if (existingUser.buildings.indexOf(b) < 0) {
          existingUser.buildings.push(b);
        }
      });
    }
    delete newUser.buildings;

    try {
      for (let prop in newUser) {
        if (!existingUser[prop] || existingUser[prop] !== newUser[prop]) {
          existingUser[prop] = newUser[prop];
        }
      }
      return existingUser;
    } catch(err) {
      response(400, err);
    }

  }

  function validateUserProps(user){
    return (user.isAdmin === undefined || !user.email || validator.validate(user.email) === false || !user.buildings|| !user.firstName || !user.lastName)
      ? false
      : true;
  }

  function cognitoCreateUser(user, admin){

    const params = {
      UserPoolId: process.env.USER_POOL_ID || 'eu-west-1_HJnusXcgZ',
      Username: user.email,
      DesiredDeliveryMediums: [
        'EMAIL'
      ],
      ForceAliasCreation: false,
      UserAttributes: [
        {
          Name: 'email',
          Value: ''
        }
      ],
      ValidationData: [
        {
          Name: 'email',
          Value: user.email
        }
      ]
    };

    return Cognito.adminCreateUser(params).promise();

  }

  function dynamoCreateUser(user){
    Dynamo.putItem({
      TableName: 'Users' + SUFFIX,
      Item: user
    }, (err, data) => {
      if (err) {
        response(400, 'Could not post user ' + user.id);
      } else {
        response(200, 'Successfully posted user ' + user.id);
      }
    })
  }

  function persistUpdatedUser(existingUser, event){
    if (event.body.buildings && event.body.body.buildings.length > 0) {
      event.body.buildings.map(b => {
        if (existingUser.buildings.indexOf(b) > -1) {
          existingUser.buildings.push(b);
        }
      });
      delete event.body.buildings;
    }
    for (let prop in event.body) {
      if (!existingUser[prop] || existingUser[prop] !== event.body[prop]){
        existingUser[prop] = newUser[prop]
       }
    }
    Dynamo.putItem({
      TableName: 'Users' + SUFFIX,
      Item: existingUser
    }, (err, data) => {
      if (err) {
        response(500, 'Unable to update user ' + existingUser.id);
      } else {
        response(200, `User ${existingUser.id} has been updated`);
      }
    });
  }

  function deleteUser(event, details){
    let orgId = details.user.orgId;
    if (!event.body || event.resource !== '/users/{userId}') {
      response(403, 'User id not specified in requeest path parameters');
    } else {
      Dynamo.getItem({

      }).promise()
        .then(user => {
          user = user.Item;
        })
    }
  }

  function validateUserUpdate(user, event, details){

    if (!user.Item || !user.Item.email) {
      response(403, 'User not found with given id');
      return;
    }
    if(event.body.email && event.body.email !== existingUser.email) {
      response(400, 'Primary key EMAIL can not be modified');
      return;
    }
    if (event.body.orgId && event.body.orgId !== existingUser.orgId) {
      response(403, 'Can not modify ORGANIZATION id');
    }
    if (details.user.isAdmin === false || details.user.isMasterAdmin === false) {
      response(401, 'User is not adminstrator and cannot update another user');
    }

    return details.user.isMasterAdmin === true
      ? masterUpdateUser(details, event.body, user)
      : adminUpdateUser(details, event.body, user);

  }

  function masterUpdateUser(details, newUser, existingUser){
    if (details.user.orgId === existingUser.orgId) {
      persistUpdatedUser(existingUser, newUser);
    }
  }

  function adminUpdateUser(details, newUser, existingUser){
    if (details.user.orgId !== existingUser.orgId) {
      response(401, 'User does not have permissions to update this user');
    }
    let allBldgsForUser = existingUser.concat(newUser.buildings ? newUser.buildings : []);
    let bldgIsPresent = false;
    allBldgsForUser.map(b => {
      if (details.user.buildings.indexOf(b) > -1) {
        bldgIsPresent = true;
      }
    });
    if (!bldgIsPresent) {
      response(401, "User to update is not registered to one of this user's buildings");
      return;
    } else {
      persistUpdatedUser(existingUser, newUser);
    }
  }

}
