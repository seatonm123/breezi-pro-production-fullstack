const validator = require('email-validator');
const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const Core = require('./core');

const SUFFIX = process.env.SUFFIX || '-matt';

// ON ADMIN POST: CAN NOT ADD OTHER ADMIN USERS

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  if (!event.headers.Authorization) {
    response(401, 'No accessToken present in request Authorization header');
  }

  Core.validateAndGetUserData(event.headers.Authorization)
    .then(details => {
      handleHttpReq(event, details);
    }, err => {
      response(err.code, err.msg);
    });

  function handleHttpReq(event, details){

    switch (event.httpMethod) {
      case 'GET':
      break;
      case 'POST':
        handlePost(event, details);
      break;
      case 'PUT':
        handlePut(event, details);
      break;
      case 'DELETE':
      break;
      default:
        response(405, 'Http method not allowed');
    }

  }

  function handlePost(event, details){
    if (!details.user.isAdmin && !details.user.isMasterAdmin) {
      response(401, 'User does not have permissions to add new users');
    }
    if (!event.body) {
      response(403, 'No user object present in request body');
    }
    let body = jsFromJson(event.body, 'email');
    if (body === null) {
      response(403, 'Request body must be valid JSON');
    }
    let userIsValid = validateUserItem(body, 'post');
    if (userIsValid.inValid) {
      response(403, userIsValid.msg);
    };
    var validForOrg = validateBldgOrg(body, details);
    if (validForOrg.inValid) {
      response(403, validForOrg.msg);
    }
    details.user.isMasterAdmin
      ? masterPost(body, details)
      : adminPost(body, details);
  }

  function handlePut(event, details){
    var userToUpdate = jsFromJson(event.body, email);
    let user = details.user;
    let valid = validateUserItem(body, 'put');
    if (valid.inValid) {
      response(403, valid.msg);
    }
    if (user.email === userToUpdate.email || event.resource !== '/users/{userId}') {
      updateUser(details, userToUpdate);
    }
    if (!details.user.isAdmin && !details.user.isMasterAdmin) {
      response(401, 'User does not have permissions to update other users');
    }
  }

  function masterPost(user, details){

    try {
      cognitoCreateUser(user)
        .then(cogRes => {
          console.log(`User ${user.email} has been instantiated in user pool`);
          dynamoCreateUser(user, details);
        });
    } catch(err) {
      response(400, err);
    }

  }

  function updateUser(details, userToUpdate){
    let user = details.user;
    userToUpdate.buildings.map(b => {
      if (details.org.buildings.indexOf(b) > -1 && details.user.buildings.indexOf(b) < 0) {
        user.buildings.push(b);
      }
    });
    delete userToUpdate.buildings;
    for (let prop in userToUpdate) {
      if (!user[prop] || user[prop] !== userToUpdate[prop]) {
        user[prop] === userToUpdate[prop];
      }
    }
    Dynamo.putItem({
      TableName: 'Users' + SUFFIX,
      Item: user
    }, (err, data) => {
      if (err) {
        response(400, err);
      } else {
        response(200, `Successfully updated user ${user.email}`);
      }
    })
  }



  function cognitoCreateUser(user){

    const params = {
      UserPoolId: process.env.USER_POOL_ID || 'eu-west-1_HJnusXcgZ',
      Username: user.email,
      DesiredDeliveryMediums: [
        'EMAIL'
      ],
      ForceAliasCreation: false,
      UserAttributes: [
        {
          Name: 'email',
          Value: ''
        },
        {
          Name: 'custom:orgId',
          Value: user.orgId
        }
      ],
      ValidationData: [
        {
          Name: 'email',
          Value: user.email
        }
      ]
    };
    return Cognito.adminCreateUser(params).promise();

  }

  function dynamoCreateUser(user, details){
    const admin = details.user;
    const org = details.org;
    user.id = uuid();
    user.orgId = details.user.orgId;
    Dynamo.putItem({
      TableName: 'Users' + SUFFIX,
      Item: user
    }, (err, data) => {
      if (err) {
        response(400, err);
      } else {
        response(200, `User ${user.id} has been instantiated in db`);
      }
    });
  }



  function validateUserItem(user, method){
    if (method === 'post') {
      if (!user.buildings || user.buildings.length < 1) {
          return {inValid: true, msg: 'At least one building id must be specified for user assignment'};
      }
    }
    if (user.isMasterAdmin){
      return {inValid: true, msg: 'Only one masterAdmin allowed per organization'};
    }
    return (user.isAdmin === undefined || !user.email || !validator.validate(user.email) || !user.buildings || !user.firstName || !user.lastName)
      ? {inValid: true, msg: 'User item not valid. Consult API documentation for valid POST request structure'}
      : true;
  }

  function validateBldgOrg(user, details){
    user.buildings.map(b => {
      if (details.org.buildings.indexOf(b) < 0) {
        return {invalid: true, msg: 'One of the requested buildings is not associated with this organization'};
      }
    });
    return true;
  }

  function jsFromJson(item, prop){
    if (item[prop]) {
      return item;
    } else {
      item = JSON.parse(item);
      if (item[prop]) {
        return item;
      } else {
        return null;
      }
    }
  }

}
