
const Core = require('./core');

const uuid = require('uuid');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {

    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Contol-Allow-Origin': '*'
      }
    });

  };

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'POST') {
    response(405, `${event.httpMethod} method not allowed`);
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      let body = validateBody(event.body);
      if (body === null) {
        response(403, 'Request body must be an array and in valid JSON format');
      }
      let byDevice = sortByDevice(body);
      persistMeasData(byDevice, org);
    }, err => {
      response(err);
    });

  function validateBody(body){

    let meas = [];
    if (Array.isArray(body)) {
      meas = body;
    } else if (Array.isArray(JSON.parse(body))) {
      meas = JSON.parse(body);
    } else {
      return null;
    }
    if (meas.every(hasDid)) {
      return meas;
    } else {
      return null;
    }
    function hasDid(m){
      return m.did;
    }

  }

  function sortByDevice(meas){
    let dids = meas.reduce((a, i) => {
      if (!a.includes(i.did)) {
        a.push(i.did);
      }
      return a;
    }, []);
    return dids.map(d => {
      let mForD = {};
      mForD = {did: d, meas: meas.filter(x => x.did === d)};
      return mForD;
    });
  }

  function persistMeasData(meas, org){

    validateOrg(meas, org)
      .then(orgMeas => {
        let noFilters = orgMeas.filter(o => o.meas.every(m => !m.airFilterId));
        validateNoOrg(noFilters)
          .then(noOrg => {
            orgMeas.concat(noOrg);
            let withoutFilter = orgMeas.filter(o => o.meas.every(m => !m.airFilterId));
            let withFilter = orgMeas.filter(o => o.meas.every(m => m.airFilterId));
            if (withoutFilter.length > 0) {
              console.log({ERROR_MESSAGE: `The following devices do not have associated airFilters, and therefore measurements were not posted: ${withoutFilter.map(w => w.device).join(', ')}`});
            }
            if (withFilter.length > 0) {
              postMeas(withFilter);
            }
          });
      });

  }

  function validateOrg(meas, org){

    return new Promise(async resolve => {

      let bldgs = await Promise.all(org.buildings.map(b => {
        return new Promise(async resolve => {
          let bldg = await Dynamo.getItem({
            TableName: 'Buildings-matt',
            Key: {
              id: b
            }
          }).promise();
          if (!bldg.Item || !bldg.Item.id) {
            resolve(null);
          } else {
            resolve(bldg.Item);
          }
        });
      }));
      let bDevs = bldgs.reduce((a, i) => {
        if (i.airVents) {
          i.airVents.map(v => {
            let currentFilter = v.airFilters.find(f => !f.toTimestamp);
            if (currentFilter !== undefined) {
              a.push({device: v.device, filter: currentFilter.id});
            }
          })
        }
        return a;
      }, []);
      meas.map(m => {
        let forDev = bDevs.find(dev => dev.device === m.did);
        if (forDev !== undefined) {
          m.meas.forEach(msmt => {
            msmt.airFilterId = forDev.filter;
          })
        }
      })
      resolve(meas);
    });
  }

  function validateNoOrg(meas){
    return new Promise(async resolve => {
      let bldgs = await Dynamo.scan({TableName: 'Buildings-matt'}).promise();
      let vents = bldgs.Items.reduce((a, i) => {
        if (i.airVents) {
          return a.concat(i.airVents);
        } else {
          return a;
        }
      }, []);
      meas.forEach(m => {
        let devVent = vents.find(v => v.device === m.did);
        if (devVent !== undefined) {
          let currentFilter = devVent.airFilters.find(f => !f.toTimestamp);
          if (currentFilter !== undefined) {
            m.meas.forEach(s => s.airFilterId = currentFilter.id);
          }
        }
      });
      resolve(meas);
    });
  }

  function postMeas(meas){
    Promise.all(meas.map(m => {
      m.meas.map(s => {
        Dynamo.putItem({
          TableName: 'Measurements-matt',
          Item: s
        }, (err, data) => {
          if (err) {
            console.log(`Measurement: did: ${s.did}, tms: ${s.tms} could not be posted`);
          } else {
            console.log(`Measurement: did ${s.did}, tms : ${s.did} posted successfully`);
          }
        })
      })
    }));
  }

};
