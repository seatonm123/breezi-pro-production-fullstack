
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let BUILDINGS = `Buildings${process.env.SUFFIX || '-matt'}`;
let DEVICES = `Devices${process.env.SUFFIX || '-matt'}`;
let MEAS = `Measurements${process.env.SUFFIX || '-matt'}`;
let DAPS = `DataAnalysisPackets${process.env.SUFFIX || '-matt'}`;

const Core = require('./core');

// GET ALL
// GET BY BUILDING
// GET BY FLOOR

exports.handler = (event, context, cb) => {

    const response = (code, msg) => {
        if (msg) console.log(JSON.stringify(msg, null, 1));
        cb(null, {
            statusCode: code,
            body: msg ? JSON.stringify(msg) : '',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
        });
    };

    if (event.httpMethod !== 'GET') {
        response(405, 'Http method not allowed');
    }

    if (!event.headers.Authorization) {
        response(401, 'AccessToken must be present in request Authorization header');
    }

    Core.getUserOrg(event.headers.Authorization)
        .then(details => {
            let admin = details.user;
            let org = details.org;
            if (!event.pathParameters) {
                wholeBundle(event, admin, org);
            } else if (event.resource === '/buildings/{buildingId}') {
                if (event.queryStringParameters && event.queryStringParameters.floor) {
                    floorBundle(event, admin, org);
                } else {
                    buildingBundle(event, admin, org);
                }
            } else {
                response(403, 'Path or query parameters invalid');
            }
        }, err => {
          console.log(err);
        });

    function wholeBundle(event, admin, org){

        if (org.buildings.length < 1) {
            response(200, {admin: admin, organization: org, buildings: []});
        }
        let bIds = org.buildings;
        Promise.all(bIds.map(b => {
            return new Promise(resolve => {
                Dynamo.getItem({
                    TableName: BUILDINGS,
                    Key: {
                        id: b
                    }
                }, (err, data) => {
                    if (err || !data.Item || !data.Item.id) {
                        resolve(null);
                    } else {
                        resolve(data.Item);
                    }
                });
            });
        })).then(buildings => {
            buildings = buildings.filter(b => b !== null);
            if (buildings.length < 1) {
                response (200, {admin: admin, organization: org, buildings: []});
            }
            buildings = buildings.map(b => {
                return constructFloors(b);
            });
            Promise.all(buildings.map(b => getBldgMeas(b))).then(withMeas => {
                response(200, {buildings: withMeas, admin: admin, org: org});
            });
        });

    }

    function buildingBundle(event, admin, org){
        let bId = event.pathParameters.buildingId;
        Dynamo.getItem({
            TableName: BUILDINGS,
            Key: {
                id: bId
            }
        }).promise()
            .then(bldg => {
                if (!bldg.Item || !bldg.Item.id) {
                    response(400, `Building ${bId} not found in database`);
                } else {
                    bldg = constructFloors(bldg.Item);
                    getBldgMeas(bldg)
                        .then(bldgMeas => {
                            response(200, bldgMeas);
                        });
                }
            });
    }

    function floorBundle(event, admin, org){
        let bId = event.pathParameters.buildingId;
        let floorIndex = parseInt(event.queryStringParameters.floor);
        Dynamo.getItem({
            TableName: BUILDINGS,
            Key: {
                id: bId
            }
        }).promise()
            .then(bldg => {
                if (!bldg.Item || !bldg.Item.id) {
                    response(400, `Building ${bId} not found in database`);
                } else {
                    bldg = constructFloors(bldg.Item);
                    let thisFloor = bldg.floors.find(f => f.index === floorIndex);
                    if (thisFloor === undefined) {
                        response(400, `Floor ${floorIndex} not found for building ${bId}`)
                    }
                    getMeasForRooms(thisFloor)
                        .then(floor => {
                            response(200, floor);
                        });
                }
            });
    }

    function constructFloors(b){
        if (b.floors.aboveGround < 1 && b.floors.belowGround < 1) {
            return b;
        } else {
            let floors = [];
            if (b.floors.belowGround > 0) {
                for (let i = 0; i < b.floors.belowGround; i++) {
                    let floor = {index: -1 - i};
                    let floorRooms = b.airVents.filter(f => f.floorIndex === -1 - i);
                    if (floorRooms.length < 1) {
                        floor.active = false;
                    } else {
                        let activeFloor = b.floors.active.find(a => a.index === -1 - i);
                        if (activeFloor !== undefined) {
                            floor.name = activeFloor.name;
                        }
                        floor.active = true;
                        floor.rooms = floorRooms;
                    }
                    floors.push(floor);
                }
            }
            for (let i = 0; i < b.floors.aboveGround; i++) {
                let floor = {index: 1 + i};
                let floorRooms = b.airVents.filter(f => f.floorIndex === 1 + i);
                if (floorRooms.length < 1) {
                    floor.active = false;
                } else {
                    let activeFloor = b.floors.active.find(a => a.index === 1 + i);
                    if (activeFloor !== undefined) {
                        floor.name = activeFloor.name;
                    }
                    floor.active = true;
                    floor.rooms = floorRooms;
                }
                floors.push(floor);
            }
            b.floors = floors;
            delete b.airVents;
            return b;
        }
    }

    function getBldgMeas(bldg){
        return new Promise((resolve, reject) => {
            if (bldg.floors.filter(f => f.active === true).length < 1) {
                resolve(bldg);
            } else {
                let activeFloors = bldg.floors.filter(f => f.active === true);
                Promise.all(activeFloors.map(f => {
                    return new Promise(resolve => {
                        getMeasForRooms(f)
                            .then(withMeas => {
                                getDapsForRooms(withMeas)
                                    .then(withDaps => {
                                        resolve(withDaps);
                                    });
                            });
                    });
                })).then(aFloors => {
                    bldg.floors.map(f => {
                        let findA = aFloors.find(a => a.index === f.index);
                        if (findA !== undefined) {
                            f = findA;
                        }
                        return f;
                    });
                    resolve(bldg);
                });
            }
        });
    }

    function getMeasForRooms(floor){
        return new Promise((resolve, reject) => {
            Promise.all(floor.rooms.map(room => {
              let params = {
                  TableName: MEAS,
                  KeyConditionExpression: 'did = :did',
                  ExpressionAttributeValues: {
                      ':did': room.device
                  },
                  Limit: 1,
                  ScanIndexForward: false
              }
              let currentFilter = room.airFilters.find(f => !f.toTimestamp);
              if (currentFilter != undefined) {
                params.KeyConditionExpression += ' AND tms >= :tms';
                params.ExpressionAttributeValues[':tms'] = currentFilter.fromTimestamp;
              }
                return new Promise(resolve => {
                    Dynamo.query(params).promise()
                        .then(meas => {
                            if (!meas.Items || meas.Items.length < 1) {
                                room.latestMeasPacket = null;
                            } else {
                                room.latestMeasPacket = meas.Items[0];
                            }
                            resolve(room);
                        });
                });
            })).then(rooms => {
                floor.rooms = rooms;
                resolve(floor);
            });
        });
    }

    function getDapsForRooms(floor){
        return new Promise((resolve, reject) => {
            Promise.all(floor.rooms.map(room => {
                return new Promise(resolve => {
                    let activeFilter = room.airFilters.find(f => !f.toTimestamp);
                    if (activeFilter === undefined) {
                        resolve(room);
                    }
                    activeFilter = activeFilter.id;
                    Dynamo.query({
                        TableName: DAPS,
                        KeyConditionExpression: 'dataAnalysisPK = :dap',
                        ExpressionAttributeValues: {
                            ':dap': activeFilter + '.DAILY_AIR_FILTER'
                        },
                        Limit: 1,
                        ScanIndexForward: false
                    }).promise()
                        .then(dap => {
                            if (!dap.Items || dap.Items.length < 1) {
                                room.latestDap = null;
                            } else {
                                room.latestDap = dap.Items[0];
                            }
                            resolve(room);
                        });
                });
            })).then(rooms => {
                floor.rooms = rooms;
                resolve(rooms);
            });
        });
    }

};
