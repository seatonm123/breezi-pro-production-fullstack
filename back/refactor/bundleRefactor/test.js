
const Index = require('./index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMiLCJldmVudF9pZCI6IjE5Nzg5ZTlmLWUzMTYtNDUxOS1hMWQ0LWVhN2Q0Mjc3YTRmMSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzI5ODIwNTEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3Mjk4NTY1MSwiaWF0IjoxNTcyOTgyMDUxLCJqdGkiOiJkZjVlZTgzMi02MzdjLTRhZTUtOTMzYS02ZjEyNzhiMjFhZGIiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMifQ.KzrLaDZQpz8PeJ5VCHewN5Et1BPu28bZWBIXtCYafE7f7P9k2p9xeLqiZCFkvhXo7Hl6hT1RILn8RebCg-hXEwhn2TNEIhX9bayt6n9IeCoqhrM1GxO0cObR0HnMaXfQKm1gRIs24uHuyQBzrlSFkNa-_zQXlPUt7X7HHUAQ8iNoDIQPZitQxRtS9EW2_s2X3Zu2AerKxT4A8ZREUPnvX29glKA21mx2ZrwSE-sqH9Hk83LoHiYvyEBP00AMEgHLZ_IhvUlDKxjcSdJEE1hWEQyYo0fS7dA0LA_4Wk9r_4ylx1Jxc_pmxRcxFnVUGhZDvCLkvaiOsTLPqtOpOkJyfA`;

const TestEvent = {
  httpMethod: 'GET',
  headers: {
    'Content-Type': 'application/json',
    Authorization: tkn
  }
}

Index.handler(TestEvent, {}, (err, res) => {
  err
    ? console.error({ERROR: err})
    : console.log({RES: res});
});
