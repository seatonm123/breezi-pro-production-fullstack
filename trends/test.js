
const Index = require('./rest/index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxNGZkZDJjZi04NTYxLTQ0NGYtYWRmNS00NDQ4ZTFhODNlNzciLCJldmVudF9pZCI6IjczOTk3YzBiLTA0MjQtNDc4Yy04MmJiLTljMWU5MGU5YTFkZCIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NjgyNDE5NTksImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU2ODI0NTU1OSwiaWF0IjoxNTY4MjQxOTU5LCJqdGkiOiI4YWY5OThmNi1lN2JiLTRmYTUtYjkzOC03N2RjZmU0NTRlZjQiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiIxNGZkZDJjZi04NTYxLTQ0NGYtYWRmNS00NDQ4ZTFhODNlNzcifQ.j7g1tQENbNxqYyBLwHIlSgxyNhiVKeWSQcnqJd5wgwSw0Qv7jDMJcHIouoTNF2s2fnRfRfWO_Iv-s6OCXBg0On3BOS5Vrbbc_p1x32wmS-VXlY1-tFY9Tfn0nv1blKoyuU1N3DKDw0fKl96b5lkRQYeU2VM_FXBwhQ_8OsaWpRx0EoNGDn_LZBw2N2pMvmqOMGNwqdWNjxADBWsS5dDdRy1uoYGSgfxNTEQ4TRoJWt_gOlBAsNjvz6QkCjtKJhiarJ8d3AsWE5XVZNAfF5qwHA1VBWwlsc_LVt3CoLBy5n7RjgPSUCOVBSo7hi0qbZ9KWf84-T4S5F_bDOTiahGOcw`;

const TestEvent = {
  httpMethod: 'GET',
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  },
  resource: '/trends/rooms/{roomId}',
  pathParameters: {
    roomId: '3d8f1953-4a63-4a91-83ac-35c22a625157'
  }
}

Index.handler(TestEvent, {}, (err, res) => {
  err ? console.console.log({ERROR: err}) : console.log(JSON.stringify(res, null, 1));
});
