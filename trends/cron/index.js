
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

exports.handler = (event, context, cb) => {

  const now = Math.floor(Date.now()/1000);

  getBldgs()
    .then(bldgs => {
      Promise.all(bldgs.map(b => {
        return new Promise(resolve => {
          getVentData(b)
            .then(ventData => {
              console.log(ventData)
            }, ventErr => {

            });
        });
      }))
    }, bldgErr => {
      emitErrorResponse(bldgErr);
    });

  async function getBldgs(){
    return new Promise(async (resolve, reject) => {
      let bldgs = await asyncScan('Buildings', []);
      resolve(bldgs);
    });
  }

  async function asyncScan(table, items, last){
    let params = {TableName: table + '-matt'};
    if (last) {
      params.ExclusiveStartKey = last;
    }
    return new Promise(async resolve => {
      let data = await Dynamo.scan(params).promise();
      if (!data || !data.Items) {
        resolve(null);
      }
      items = items.concat(data.Items);
      if (data.LastEvaluatedKey) {
        return asyncScan(table, items, data.LastEvaluatedKey);
      } else {
        resolve(data.Items);
      }
    });
  }

  function getVentData(bldg){
    return new Promise((resolve, reject) => {
      bldg.airVents.map(async v => {
        let ventMeas = await getVentMeas(v);
        if (ventMeas !== null) {
          ventMeas = getMeasAvgs(ventMeas);
        }
        let ventDap = await getVentDap(v);
        if (ventDap !== null) {
          let accum = {days: 0, percent: null};
          ventDap.map(d => {
            accum.days += d.values.lifeExpectancy;
            if (d.values.remainingPercents) {
              if (accum.percent === null) {
                accum.percent = d.values.remainingPercents;
              } else {
                accum.percent += d.values.remainingPercents;
              }
            }
          });
          accum.days = Math.round(accum.days/ventDap.length);
          accum.percent = Math.round(accum.percent/ventDap.length);
          let final = {
            airVentId: v.id,
            buildingId: bldg.id,
            deviceId: v.device,
            meas: ventMeas,
            dap: accum,
            tms: now
          }
          Dynamo.putItem({TableName: 'DailyTrends-matt', Item: final}, (err, data) => {
            if (err) {
              console.log(err)
            } else {
              console.log('Success')
            }
          })
        }
      });
    });
  }

  function getVentMeas(vent){
    return new Promise(resolve => {
      let params = {
        TableName: 'Measurements-matt',
        KeyConditionExpression: 'did = :did AND tms >= :tms',
        ExpressionAttributeValues: {
          ':did': vent.device,
          ':tms': now - 86400
        },
        ScanIndexForward: false
      }
      Dynamo.query(params, (err, data) => {
        if (err || !data.Items || data.Items.length < 1) {
          resolve(null);
        } else {
          resolve(data.Items);
        }
      });
    });
  }

  function getMeasAvgs(measurements){
    let meas = measurements.reduce((a, i, n, r) => {
      for (let prop in i) {
        if (['fwv', 'did', 'airFilterId', 'tms', 'dat'].indexOf(prop) < 0) {
          if (a[prop]) {
            a[prop] += i[prop];
          } else {
            a[prop] = i[prop]
          }
        }
      }
      for (let prop in i.dat) {
        if (a[prop]) {
          a[prop] += calcAvg(i.dat[prop]);
        } else {
          a[prop] = calcAvg(i.dat[prop]);
        }
      }
      return a;
    }, {});
    for (let prop in meas) {
      meas[prop] = Math.round(meas[prop]/measurements.length);
    }
    return meas;
  }

  function getVentDap(vent){
    return new Promise(resolve => {
      if (!vent.airFilters) {
        resolve(null);
      }
      let filter = vent.airFilters.find(f => !f.toTimestamp);
      if (filter === undefined) {
        resolve(null);
      }
      let params = {
        TableName: 'DataAnalysisPackets-matt',
        KeyConditionExpression: 'dataAnalysisPK = :dap AND #tms >= :tms',
        ExpressionAttributeNames: {
          '#tms': 'timestamp'
        },
        ExpressionAttributeValues: {
          ':dap': filter.id + '.DAILY_AIR_FILTER',
          ':tms': now - 86400
        },
        ScanIndexForward: false
      }
      Dynamo.query(params, (err, data) => {
        if (err || !data.Items || data.Items.length < 1) {
          resolve(null);
        } else {
          resolve(data.Items);
        }
      });
    });
  }

  function calcAvg(arr){
    let total = arr.reduce((a, i) => {
      a += i;
      return a;
    }, 0);
    return (Math.round(total/arr.length));
  }

};
