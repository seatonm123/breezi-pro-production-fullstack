
const uuid = require('uuid');
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var TABLE = process.env.TABLE;
var SUFFIX = process.env.SUFFIX;

// DON'T FORGET TO ADD CO2

async function handler(){
  // let bldg = await Dynamo.getItem({TableName: 'Buildings-matt', Key: {id: 'bfc61129-18fd-4b7f-9ee3-bd7d43b9c130'}}).promise();
  // bldg = bldg.Item;
  // fs.writeFile('./jsons/building.json', JSON.stringify(bldg, null, 1), (err, res) => {
  //   err ? console.log({ERROR: err}) : console.log('success');
  // });

  // let bldg = require('./jsons/building');
  // console.log(bldg);
  // let counter = -1;
  // let final = [];
  // Promise.all(bldg.airVents.map(async v => {
  //   counter += 1;
  //   let meas = require(`./jsons/meas${counter.toString()}`);
  //   let filter = v.airFilters.find(f => !f.toTimestamp).id;
  //   meas = meas.Items.map(m => {
  //     m.did = v.device;
  //     m.tms = Math.round(Date.now()/1000) - (counter * 3600);
  //     m.airFilterId = filter;
  //     if (!m.dat.co2) {
  //       m.dat.co2 = [Math.round(Math.random()*5000)];
  //     }
  //     return m;
  //   });
  //   final = final.concat(meas);
  //   // Promise.all(meas.map(m => {
  //   //   Dynamo.putItem({TableName: 'Measurements-matt', Item: m}, (err, data) => {
  //   //     if (err) {
  //   //       console.log({ERROR: err});
  //   //     } else {
  //   //       console.log('success ' + v.id);
  //   //     }
  //   //   })
  //   // }));
  // }));
  // fs.writeFile('./jsons/final.json', JSON.stringify(final, null, 1), (err, res) => {
  //   if (err) {
  //     console.log({ERROR: err});
  //   } else {
  //     console.log('Success');
  //   }
  // })

  // let final = require('./jsons/final');
  // final.map(async f => {
  //   Dynamo.putItem({TableName: 'Measurements-matt', Item: f}, (err, data) => {
  //     if (err) {
  //       console.log({ERROR: err});
  //     } else {
  //       console.log('Success!')
  //     }
  //   })
  // })

  // let bldg = require('./jsons/building');
  // let devices = bldg.airVents.map(v => {return {dev: v.device, filter: v.airFilters.find(f => !f.toTimestamp).id}});
  // let meas = [];
  // for (let i = 0; i < 23; i++) {
  //   let theseMeas = require(`./jsons/meas${i.toString()}`);
  //   if (theseMeas.Items.length > 100) {
  //     meas.push(theseMeas);
  //   }
  // }
  // meas = meas.map(m => m.Items.splice(0, 50));
  // devices.map(d => {
  //   let forDev = meas[devices.indexOf(d)];
  //   forDev.map(f => {
  //     f.did = d.dev;
  //     f.tms = Math.round(Date.now()/1000) - (forDev.indexOf(f) * 3600);
  //     f.airFilterId = d.filter;
  //     if (!f.dat.co2) {
  //       f.dat.co2 = [];
  //       while (f.dat.co2.length < 10) {
  //         f.dat.co2.push(Math.round(Math.random() * 5000));
  //       }
  //     }
  //     Dynamo.putItem({TableName: 'Measurements-matt', Item: f}, (err, data) => {
  //       if (err) {
  //         console.log({ERROR: err});
  //       } else {
  //         console.log('SUCCESS ' + d.dev);
  //       }
  //     })
  //   });
  // });

  let daps = [];
  for (let i = 0; i < 23; i++) {
    let d = require(`./jsons/dap${i.toString()}`);
    daps.push(d);
  }

  daps = daps.filter(d => d.length > 0);

  let bldg = require('./jsons/building');
  let filters = bldg.airVents.map(v => v.airFilters.find(f => !f.toTimestamp).id);
  filters.map(f => {
    let rIndex = Math.floor(Math.random() * 11);
    let fDap = daps[rIndex];
    fDap.map(d => {
      d.dataAnalysisPK = f + '.DAILY_AIR_FILTER',
      d.id = uuid()
      d.objectId = f;
      d.timestamp = Math.round(Date.now()/1000) - (Math.round(86400 * fDap.indexOf(f)));
      d.userId = '8b43d91e-9773-4af6-92ed-f51dd4c8d674';
      if (!d.values.remainingPercents) {
        d.values.remainingPercents = Math.floor(Math.random()*100);
      }
      if (!d.values.maximumDeltaPressure95Percentile) {
        d.values.maximumDeltaPressure95Percentile = Math.random() * 100;
      }
      Dynamo.putItem({TableName: 'DataAnalysisPackets-matt', Item: d}, (err, data) => {
        if (err) {
          console.log({ERROR: err});
        } else {
          console.log('Success ' + d.id);
        }
      });
    });
  });

}

handler();
