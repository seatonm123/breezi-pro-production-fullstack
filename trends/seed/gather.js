
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

async function handler(){
  // let devices = await Dynamo.scan({
  //   TableName: 'Devices-pilot',
  //   Limit: 24
  // }).promise();
  // fs.writeFile('./jsons/devices.json', JSON.stringify(devices.Items, null, 1), (err, data) => {
  //   err ? console.log({ERROR: err}) : console.log('Success');
  // });

  // let devices = require('./jsons/devices');
  //
  // Promise.all(devices.map(async d => {
  //   let filePath = `meas${devices.indexOf(d)}`;
  //   let params = {
  //     TableName: 'Measurements-pilot',
  //     KeyConditionExpression: 'did = :did',
  //     ExpressionAttributeValues: {
  //       ':did': d.deviceId
  //     },
  //     Limit: 200,
  //     ScanIndexForward: false
  //   };
  //   let forThisDevice = await Dynamo.query(params).promise();
  //   fs.writeFile(`./jsons/${filePath}.json`, JSON.stringify(forThisDevice, null, 1), (err, res) => {
  //     err ? console.log({ERROR: err}) : console.log('successfully wrote device ' + d.deviceId);
  //   });
  // }));

  // let bldg = await Dynamo.getItem({TableName: 'Buildings-pilot', Key: {id: '76a5ca68-3d69-4c61-9978-692d4f7ec79d'}}).promise();
  // fs.writeFile('./jsons/bldg.json', JSON.stringify(bldg.Item, null, 1), (err, res) => {
  //   err ? console.error({ERROR: err}) : console.log('Success');
  // });

  let filters = [];
  for (let i = 0; i < 23; i++) {
    let m = require(`./jsons/meas${i.toString()}`);
    filters.push(m.Items[0].airFilterId);
  }

  Promise.all(filters.map(async f => {
    let params = {
      TableName: 'DataAnalysisPackets-pilot',
      KeyConditionExpression: 'dataAnalysisPK = :dap',
      ExpressionAttributeValues: {
        ':dap': f + '.DAILY_AIR_FILTER'
      },
      Limit: 10,
      ScanIndexForward: false
    }
    let daps = await Dynamo.query(params).promise();
    fs.writeFile(`./jsons/dap${filters.indexOf(f).toString()}.json`, JSON.stringify(daps.Items, null, 1), (err, res) => {
      err ? console.log({ERROR: err}) : console.log('SUCCESS ' + f);
    })
  }));

}

handler();
