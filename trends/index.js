
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

exports.handler = (event, context, cb) => {

  asyncGet('Buildings', [])
    .then(buildings => {
      buildings = buildings.filter(b => b.airVents);
      
    });

  function asyncGet(table, items, last){
    return new Promise(async (resolve, reject) => {
      let params = {
        TableName: `${table}-integration`,
      };
      if (last) {
        params.ExclusiveStartKey = last;
      }
      let data = await Dynamo.scan(params).promise();
      if (!data || !data.Items) {
        reject({code: 400, msg: `No data found in ${table} table`});
      } else {
        items = items.concat(data.Items);
        if (data.LastEvaluatedKey) {
          return asyncGet(table, items, data.LastEvaluatedKey);
        } else {
          resolve(items);
        }
      }
    });
  }

  function getDevices(){
    return new Promise(async (resolve, reject) => {
      let devices = await getAllDevices([]);
      if (devices.length > 0) {
        resolve(devices);
      } else {
        reject({code: 400, msg: 'No devices found'});
      }
    });
  }

  async function getAllDevices(devs, last){
    let params = {
      TableName: 'Devices-integration'
    };
    if (last) {
      params.ExclusiveStartKey = last;
    }
    let data = await Dynamo.scan(params).promise();
    devs = devs.concat(data.Items);
    if (data.LastEvaluatedKey) {
      return getAllDevices(devs, data.LastEvaluatedKey);
    } else {
      return devs;
    }
  }

  async function getBldgs(devices){
    return new Promise((resolve, reject) => {
      let
    });
  }

};
