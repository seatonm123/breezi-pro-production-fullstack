
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const Core = require('./core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  };

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'GET') {
    response(405, 'Http method not allowed');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      let admin = details.user;
      let org = details.org;
      switch (event.resource) {
        case '/trends/rooms/{roomId}':
          trendsByRoom(event, admin, org);
        break;
        case '/trends/floors/{floorId}':
        break;
        case '/trends/buildings/{buildingId}':
        break;
        default:
          response(403, 'Room, floor or building id must be included in request path parameters');
      }
    });

  function trendsByRoom(event, admin, org){
    let roomId = event.pathParameters.roomId;
    validateOwnsRoom(roomId, org)
      .then(room => {
        console.log(room);
        Dynamo.query({
          TableName: 'DailyTrends-matt',
          KeyConditionExpression: 'airVentId = :vId',
          ExpressionAttributeValues: {
            ':vId': room
          },
          Limit: 50,
          ScanIndexForward: false
        }, (err, data) => {
          if (err) {
            console.log(err);
            response(400, 'Error getting data for room ' + room);
          } else if (!data.Items || data.Items.length < 1) {
            response(400, 'No data found for room ' + room);
          } else {
            let res = {
              airVentId: room,
              buildingId: data.Items[0].buildingId,
              deviceId: data.Items[0].deviceId
            };
            data.Items.map(d => {
              delete d.airVentId;
              delete d.buildingId;
              delete d.deviceId;
              return d;
            });
            res.data = data.Items;
            response(200, res);
          }
        });
      }, roomErr => {
        response(roomErr.code, roomErr.msg);
      });
  }

  async function validateOwnsRoom(id, org){
    console.log(org.buildings);
    let orgBldgs = await Promise.all(org.buildings.map(async b => {
      let bldg = await Dynamo.getItem({
        TableName: 'Buildings-matt',
        Key: {
          id: b
        }
      }).promise();
      if (bldg.Item) {
        return Promise.resolve(bldg.Item);
      } else {
        return Promise.resolve(null);
      }
    }));
    orgBldgs = orgBldgs.filter(oB => oB !== null);
    let vents = orgBldgs.map(b => b.airVents.map(v => v.id)).reduce((a, i) => {
      return a.concat(i);
    }, []);
    if (vents.indexOf(id) < 0) {
      return Promise.reject({code: 401, msg: `Vent ${id} belongs to a different organization`});
    } else {
      return Promise.resolve(id);
    }
  }

};
