import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/login/Splash';
import Org from '@/components/monitor/Org';
import AddBldg from '@/components/onboard/Building';
import Building from '@/components/monitor/Building';
import AddRoom from '@/components/onboard/Room';
import Room from '@/components/monitor/room';
import Loading from '@/components/util/Loading';
// import Monitor from '@/components/monitor/Monitor'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/org',
      name: 'Org',
      component: Org
    },
    {
      path: '/newBuilding',
      name: 'AddBldg',
      component: AddBldg
    },
    {
      path: '/building',
      name: 'Building',
      component: Building
    },
    {
      path: '/newRoom',
      name: 'AddRoom',
      component: AddRoom
    },
    {
      path: '/room',
      name: 'Room',
      component: Room
    },
    {
      path: '/loading',
      name: 'Loading',
      component: Loading
    }
  ]
});
