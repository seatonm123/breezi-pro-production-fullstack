
const fs = require('fs');

function handler(){

  let example = new WorkOrder(
    'email/id',
    Math.floor(Date.now() / 1000),
    ['list', 'of', "recipients'", 'ids'],
    ['{predetermined}', '{list}', '{of}', '{WO}', '{types}'],
    'max50Char',
    'max160Char',
    'buildingId',
    'airVentId',
    'floorIndex(INTEGER)',
    'int1Through3'
  );

  fs.writeFile('./wo.json', JSON.stringify(example, null, 1), (err, data) => {
    err ? console.error({ERROR: err}) : console.log('Success');
  });

}

class WorkOrder {
  constructor(senderId, tms, recipients, types, subject, notes, bId, vId, fIx, urgency){
    this.senderId = senderId;
    this.tms = tms;
    this.recipients = recipients;
    this.types = types;
    this.subject = subject;
    this.notes = notes;
    this.building = bId;
    this.vent = vId;
    this.floor = fIx;
    this.urgency = urgency;
  }
}

handler();
