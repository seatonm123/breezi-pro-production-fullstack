
const uuid = require('uuid');
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

let email = 'seatonm123@gmail.com';

async function handler(email){

  let origMeas = require('./meas');

  // let user = await Dynamo.getItem({
  //   TableName: 'Users-matt',
  //   Key: {
  //     email: email
  //   }
  // }).promise();
  //
  // user = user.Item;
  //
  // let org = await Dynamo.getItem({
  //   TableName: 'Organizations-matt',
  //   Key: {
  //     name: user.org
  //   }
  // }).promise();
  //
  // org = org.Item;
  // //
  // let bldg = await Dynamo.getItem({
  //   TableName: 'Buildings-matt',
  //   Key: {
  //     id: org.buildings[0]
  //   }
  // }).promise();
  //
  // bldg = bldg.Item;
  //
  // let filters = bldg.airVents.map(v => v.airFilters[0].id);
  //
  // fs.writeFile('./filters.json', JSON.stringify(filters, null, 1), (err, res) => {
  //   err ? console.error(err) : console.log('Success');
  // });
  //
  // let devices = bldg.airVents.map(v => v.device);
  //
  // fs.writeFile('./devices.json', JSON.stringify(devices, null, 1), (err, data) => {
  //   err ? console.error(err) : console.log('success');
  // })

  let devices = require('./devices');



  class Meas {
    constructor(quality){
      this.quality = quality;
      this.ctr = Math.round(Math.random() * 11);
      this.sig = randomNeg(Math.round(Math.random()*500), Math.round(Math.random()*500) * -1);
      this.bat = Math.round(Math.random() * 30) + 45;
      this.fwv = '0.1.004';
      this.tms = Math.round(Date.now()/1000);
      // this.tms = Math.round(Date.now()/1000) - Math.round(Math.random() * (604800));
      this.per = Math.round(Math.random() * 11);
      this.dat = calcDat(this.quality);
    }

  }

  let measurements = devices.map(d => {
    let dMeas = new Meas(ofThree());
    dMeas.did = d;
    return dMeas;
  });

  measurements.map(m => {
    Dynamo.putItem({TableName: 'Measurements-matt', Item: m}, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log('successfully posted ' + m.did);
      }
    })
  });

//   class Dap {
//     constructor(quality, fId){
//       this.quality = quality;
//       this.id = uuid();
//       this.timestamp = Math.round(Date.now()/1000) - (Math.random() * 604801);
//       this.type = 'DAILY_AIR_FILTER';
//       this.userId = '8b43d91e-9773-4af6-92ed-f51dd4c8d674';
//       this.objectId = fId;
//       this.dataAnalysisPK = fId + '.DAILY_AIR_FILTER',
//       this.values = calcDap(this.quality)
//     }
//   }
//
//   function calcDap(quality){
//     switch (quality) {
//       case 'crit':
//         return {
//           deltaPressure95Percentile: Math.round(Math.random() * 45) + 50,
//           lifeExpectancy: Math.round(Math.random() * 5),
//           maximumDeltaPressure95Percentile: Math.round(Math.random() * 45) + 50,
//           remainingPercents: Math.round(Math.random() * 10) + 5
//         }
//       break;
//       case 'bad':
//       return {
//         deltaPressure95Percentile: Math.round(Math.random() * 25) + 20,
//         lifeExpectancy: Math.round(Math.random() * 10) + 6,
//         maximumDeltaPressure95Percentile: Math.round(Math.random() * 25) + 20,
//         remainingPercents: Math.round(Math.random() * 35) + 15
//       }
//       break;
//       case 'good':
//       return {
//         deltaPressure95Percentile: Math.round(Math.random() * 19),
//         lifeExpectancy: Math.round(Math.random() * 164) + 16,
//         maximumDeltaPressure95Percentile: Math.round(Math.random() * 19),
//         remainingPercents: Math.round(Math.random() * 50) + 50
//       }
//       break;
//     }
//   }
//
//   const filters = require('./filters');
//
//   filters.map(f => {
//     let fDap = new Dap(ofThree(), f);
//     Dynamo.putItem({
//       TableName: 'DataAnalysisPackets-matt',
//       Item: fDap
//     }, (err, data) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log('successfully posted ' + f);
//       }
//     })
//   });
// //   //
  function ofThree(){
    let rando = Math.floor(Math.random() * 11);
    if (rando >= 4) {
      return 'good';
    } else if (rando >= 2) {
      return 'bad'
    } else {
      return 'crit';
    }
  }
  //
  function randomNeg(is, isnt){
    return Math.floor(Math.random() * 2) === 1 ? is : isnt;
  }
//
  function calcDat(q){
    let res = {};
    switch (q) {
      case 'crit':
        res = {
          ttp: [(Math.random()*5).toFixed(2) + 31.5],
          hhm: [Math.floor(Math.random() * 2) === 1]
            ? [Math.round(Math.random() * 20)]
            : [Math.round(Math.random() * 20) + 80],
          iaq: [Math.round(Math.random() * 250) + 250],
          co2: [Math.round(Math.random() * 400) + 100],
          run: [Math.floor(Math.random() * 2)]
        }
      break;
      case 'bad':
      res = {
        ttp: [(Math.random()*5).toFixed(2) + 26.5],
        hhm: [Math.round(Math.random() * 40) + 40],
        iaq: [Math.round(Math.random() * 150) + 100],
        co2: [Math.round(Math.random() * 150) + 350],
        run: [Math.floor(Math.random() * 2)]
      }
      break;
      case 'good':
      res = {
        ttp: [(Math.random() * 8).toFixed(2) + 18],
        hhm: [Math.round(Math.random() * 20) + 20],
        iaq: [Math.round(Math.random() * 100)],
        co2: [Math.round(Math.random() * 349)],
        run: [Math.floor(Math.random() * 2)]
      }
      break;
    }
  return res;
  }
// //
}

handler(email);

// {
//  "dat": {
//   "new": [
//    0,
//    0,
//    0,
//    0,
//    0,
//    0
//   ],
//   "htp": [
//    -0.08,
//    -0.03,
//    -0.01,
//    -0.01,
//    -0.01,
//    0
//   ],
//   "co2": [
//    12,
//    57,
//    50,
//    223
//   ],
//   "dpr": [
//    409,
//    305,
//    250,
//    235,
//    220,
//    221
//   ],
//   "iAC": [
//    0,
//    0,
//    0,
//    0,
//    0,
//    0
//   ],
//   "run": [
//    0,
//    0,
//    0,
//    0,
//    0,
//    0
//   ],
//   "hpr": [
//    96575,
//    96422,
//    96410,
//    96404,
//    96392,
//    96392
//   ],
//   "voc": [
//    100208,
//    100208,
//    100208,
//    100208,
//    100208,
//    100208
//   ],
//   "hhm": [
//    33,
//    28,
//    26,
//    25,
//    24,
//    23
//   ],
//   "oAC": [
//    0,
//    0,
//    0,
//    0,
//    0,
//    0
//   ],
//   "ttp": [
//    26.33,
//    26.42,
//    26.49,
//    26.53,
//    26.55,
//    26.56
//   ],
//   "tpr": [
//    100974,
//    100972,
//    100972,
//    100973,
//    100974,
//    100973
//   ],
//   "iaq": [
//    25,
//    25,
//    25,
//    25,
//    25,
//    25
//   ],
//   "vAC": [
//    0,
//    0,
//    0,
//    0,
//    0,
//    0
//   ]
//  },
//  "airFilterId": "135f6350-2b79-445f-be94-a2f3f19656ee",
//  "did": 8675309
// }
