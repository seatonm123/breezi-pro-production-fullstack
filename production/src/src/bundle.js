
import moment from 'moment';
import convert from 'convert-units';

import Thresh from '@/lib/thresholds';

class alertSchema {
  constructor(){
    this.days = {
      bad: 0,
      crit: 0
    };
    this.iaq = {
      bad: 0,
      crit: 0
    };
    this.ttp = {
      bad: 0,
      crit: 0
    };
    this.hhm = {
      bad: 0,
      crit: 0
    };
    this.co2 = {
      bad: 0,
      crit: 0
    }
  }
}

export default {

  formatBundle(bundle){
    if (!bundle.buildings || bundle.buildings.length < 1) {
      return bundle;
    } else {
      bundle.buildings = getAlertAvg(bundle.buildings);
      return bundle;
    }
  },

  switchUnits(units, buildings){
    buildings = buildings.reduce((a, i, n, r) => {
      i.floors.forEach(f => {
        if (f.rooms) {
          f.rooms.forEach(r => {
            console.log(r);
            let size = r.altSize ? r.altSize : r.modSize;
            size = size.split('x');
            if (units === 'metric') {
              r.altSize = size.map(s => convert(parseInt(s)).from('in').to('cm')).join('x');
              if (r.rdgs && r.rdgs.ttp && r.rdgs.ttp !== '-') {
                console.log(r.rdgs)
                r.rdgs.ttp = convert(r.rdgs.ttp).from('F').to('C');
              }
            } else {
              r.altSize = size.map(s => convert(parseInt(s).from('cm').to('in'))).join('x');
              if (r.rdgs && r.rdgs.ttp && r.rdgs.ttp !== '-') {
                r.rdgs.ttp = convert(r.rdgs.ttp).from('C').to('F');
              }
            }
          });
        }
        a.push(i);
      });
      return a;
    }, []);
  },

  goodBadCrit(prop, val){
    let params = Thresh[prop];
    for (let prop in params) {
      if (params[prop].outlier && val === params[prop].outlier) {
        return prop;
      } else {
        if (val >= params[prop].low && val <= params[prop].high) {
          return prop.replace('2', '');
        }
      }
    }
  }

}

function getAlertAvg(bldgs){

  bldgs.forEach(bldg => {
    accumBldgAlerts(bldg);
  });

  let alerts = new alertSchema();
  let avg = bldgs.reduce((a, i) => {
    for (let prop in i.avg) {
      if (!isNaN(i.avg[prop])) {
        if (!a[prop]) {
           a[prop] = {val: i.avg[prop], count: 1};
        } else {
           a[prop].val += i.avg[prop];
           a[prop].count ++;
        }
      }
      if (i.alerts[prop].bad) {
        a[prop].bad = !a[prop].bad
          ? i.alerts[prop].bad
          : i.alerts[prop].bad + a[prop].bad
      }
      if (i.alerts[prop].crit) {
        a[prop].crit = !a[prop].crit
          ? i.alerts[prop].crit
          : i.alerts[prop].crit + a[prop].crit;
      }
    }
    return a;
  }, {});

  for (let prop in avg){
    if (avg[prop].bad) {
      alerts[prop].bad = avg[prop].bad;
    }
    if (avg[prop].crit) {
      alerts[prop].crit = avg[prop].crit;
    }
    avg[prop] = Math.round(avg[prop].val / avg[prop].count);
  }

  let org = {
    alerts: alerts,
    avg: avg,
    overallAlerts: aggAlerts(alerts),
    buildings: bldgs
  }

  return org;

}

function aggAlerts(alerts){
  let a = {bad: 0, crit: 0};
  for (let prop in alerts) {
    a.bad += alerts[prop].bad;
    a.crit += alerts[prop].crit;
  }
  return a;
}

function accumBldgAlerts(bldg){

  if (!bldg.floors || bldg.floors.length < 1) {
    return {data: bldg, alerts: new alertSchema()};
  }
  bldg.floors.forEach(floor => {
    accumFloorAlerts(floor);
  });

  bldg.avg = bldg.floors.reduce((a, i, x, r) => {
    for (let prop in i.avg) {
      if (!isNaN(i.avg[prop])) {
        a[prop].val += i.avg[prop];
        a[prop].count ++;
      }
    }
    if (x === r.length - 1) {
      for (let prop in a) {
        if (a[prop].val !== 0) {
          a[prop] = Math.round(a[prop].val/a[prop].count)
        } else {
          a[prop] = 0;
        }
      }
    }
    return a;
  }, {
    days: {val: 0, count: 0},
    ttp: {val: 0, count: 0},
    hhm: {val: 0, count: 0},
    iaq: {val: 0, count: 0},
    co2: {val: 0, count: 0},
  });

  bldg.alerts = bldg.floors.reduce((a, i) => {

    if (i.alerts) {
      for (let prop in i.alerts) {
        a[prop].bad += i.alerts[prop].bad;
        a[prop].crit += i.alerts[prop].crit;
      }
    }
    return a;
  }, {
    days: {bad: 0, crit: 0},
    ttp: {bad: 0, crit: 0},
    hhm: {bad: 0, crit: 0},
    iaq: {bad: 0, crit: 0},
    co2: {bad: 0, crit: 0},
  });
  bldg.overallAlerts = aggAlerts(bldg.alerts);

  return bldg;

}

function accumFloorAlerts(floor){

  if (!floor.active || !floor.rooms || floor.rooms.length < 1) {
    return {data: floor, alerts: new alertSchema()};
  }

  floor.rooms.forEach(room => {
    accumRoomAlerts(room);
  });

  floor.avg = floor.rooms.reduce((a, i, x, r) => {
    for (let prop in i.rdgs) {
      if (!isNaN(i.rdgs[prop])) {
        a[prop].val += i.rdgs[prop];
        a[prop].count ++;
      }
    }
    if (x === r.length - 1) {
      for (let prop in a) {
        if (a[prop].val !== 0) {
          a[prop] = Math.round(a[prop].val/a[prop].count)
        } else {
          a[prop] = 0;
        }
      }
    }
    return a;
  }, {
    days: {val: 0, count: 0},
    ttp: {val: 0, count: 0},
    hhm: {val: 0, count: 0},
    iaq: {val: 0, count: 0},
    co2: {val: 0, count: 0},
  });

  floor.alerts = floor.rooms.reduce((a, i) => {

    if (i.alerts) {
      for (let prop in i.alerts) {
        a[prop].bad += i.alerts[prop].bad;
        a[prop].crit += i.alerts[prop].crit;
      }
    }
    return a;
  }, {
    days: {bad: 0, crit: 0},
    ttp: {bad: 0, crit: 0},
    hhm: {bad: 0, crit: 0},
    iaq: {bad: 0, crit: 0},
    co2: {bad: 0, crit: 0},
  });
  floor.overallAlerts = aggAlerts(floor.alerts);
  return floor;

}

function accumRoomAlerts(room){
  let roomRdgs = {
    days: '-',
    iaq: '-',
    ttp: '-',
    hhm: '-',
    co2: '-'
  }
  if (room.latestDap !== null) {
    let days = room.latestDap.values.lifeExpectancy;
    if (days === -1) {
      days = 180;
    } else if (days === -2) {
      days = 0;
    }
    roomRdgs.days = Math.round(room.latestDap.values.lifeExpectancy);
    room.changed = moment(room.latestDap.timestamp * 1000).format('LLL');
  }
  if (room.latestMeasPacket !== null) {
    room.airFilter = room.latestMeasPacket.airFilterId;
    delete room.latestMeasPacket.airFilterId;
    room.updated = moment(room.latestMeasPacket.tms * 1000).format('LLL');
    for (let prop in room.latestMeasPacket.dat) {
      if (roomRdgs[prop]) {
        roomRdgs[prop] = Math.round(room.latestMeasPacket.dat[prop][0]);
      }
    }
  }
  room.rdgs = roomRdgs;
  for (let prop in room) {
    if (['ac', 'active', 'airFilters', 'aq', 'floorIndex', 'latestDap', 'latestMeasPacket'].includes(prop)) {
      delete room[prop];
    }
  }
  try {
    let activeFilter = room.airFilters.find(f => !f.toTimestamp);
    room.lastChanged = moment(activeFilter.fromTimestamp * 1000).format('L');
  } catch(err) {
    room.lastChanged = '-';
  }
  room.alerts = roomAlerts(room.rdgs);
  room.overallAlerts = aggAlerts(room.alerts);
  return room;
}

function roomAlerts(rdgs){
  let schema = new alertSchema();
  for (let prop in rdgs) {
    if (!isNaN(rdgs[prop])) {
      let rdg = rdgs[prop];
      let params = Thresh[prop];
      if (rdg >= params.bad.low && rdg <= params.bad.high) {
        schema[prop].bad ++;
      }
      if (params.bad2 && rdg >= params.bad2.low && rdg <= params.bad2.high) {
        schema[prop].bad ++;
      }
      if (rdg >= params.crit.low && rdg <= params.crit.high) {
        schema[prop].crit ++;
      }
      if (params.crit2 && rdg >= params.crit2.low && rdg <= params.crit2.high) {
        schema[prop].crit ++;
      }
    }
  }

  return schema;
}

function hasAlert(prop, rdg){
  let params = Thresh[prop];
  if (rdg >= params.bad.low && rdg <= params.bad.high) {
    return 'bad';
  }
  if (params.bad2 && rdg >= params.bad2.low && rdg <= params.bad2.high) {
    return 'bad';
  }
  if (rdg >= params.crit.low && rdg <= params.crit.high) {
    return 'crit';
  }
  if (params.crit2 && rdg >= params.crit2.low && rdg <= params.crit2.high) {
    return 'crit';
  }
  return null;
}
