
const moment = require('moment');

export default () => {

  let now = Math.round(Date.now()/1000);
  let days = [now];
  do {
    days.push(days[days.length - 1] - 86400);
  } while (days.length < 10);

  let minMax = {
    days: {min: 50, max: 80},
    ttp: {min: 17, max: 29},
    hhm: {min: 20, max: 85},
    iaq: {min: 25, max: 120},
    co2: {min: 100, max: 250}
  }

  let dayStart = setParams(minMax.days.min, minMax.days.max);
  let ttpStart = setParams(minMax.ttp.min, minMax.ttp.max);
  let hhmStart = setParams(minMax.hhm.min, minMax.hhm.max);
  let iaqStart = setParams(minMax.iaq.min, minMax.iaq.max);
  let co2Start = setParams(minMax.co2.min, minMax.co2.max);

  let dayTrends = setTen(minMax.days, [dayStart]);
  let ttpTrends = setTen(minMax.ttp, [ttpStart]);
  let hhmTrends = setTen(minMax.hhm, [hhmStart]);
  let iaqTrends = setTen(minMax.iaq, [iaqStart]);
  let co2Trends = setTen(minMax.co2, [co2Start]);

  let trends = days.map((d, x) => {
    return {
      tms: d,
      dap: {
        days: dayTrends[x]
      },
      meas: {
        ttp: ttpTrends[x],
        hhm: hhmTrends[x],
        iaq: iaqTrends[x],
        co2: co2Trends[x]
      }
    }
  });

  return trends;

}

function setParams(low, high){
  let dif = high - low;
  return Math.round(Math.random() * dif) + low;
}

function setTen(thresh, arr) {
  if (arr.length === 10) {
    return arr;
  } else {
    let last = arr[arr.length - 1];
    arr.push(nextInterval(last, thresh));
    return setTen(thresh, arr);
  }
}

function nextInterval(cI, thresh){

  let interval = Math.round((thresh.max - thresh.min) / 10);
  let plusMin = (Math.random() * 2 > 1);
  let newFinal;
  if (!plusMin) {
    newFinal = cI - Math.floor(Math.random() * interval);
  } else {
    newFinal = cI + Math.floor(Math.random() * interval);
  }

  return (newFinal >= thresh.min && newFinal <= thresh.max)
    ? newFinal
    : nextInterval(cI, thresh);

}

class trendPoint {
  constructor(days, ttp, hhm, iaq, co2){

  }
}
