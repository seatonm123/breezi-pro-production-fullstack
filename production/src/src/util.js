
let countries = require('../lib/countries');
import cities from 'full-countries-cities';

export default {

  matchCountry(c){
    let theseCountries = Object.keys(countries).map(k => countries[k]);
    if (c === '') {
      return theseCountries;
    } else {
      theseCountries = theseCountries.filter(i => {
        return i.name.toLowerCase().includes(c.toLowerCase());
      });
      return theseCountries;
    }
  },

  matchCity(c, input){
    let theseCities = cities.getCities(c);
    if (input === '') {
      return theseCities;
    } else {
      theseCities = theseCities.filter(i => {
        return i.toLowerCase().includes(input.toLowerCase());
      });
      return theseCities;
    }
  }

}
