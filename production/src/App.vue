<template>
  <div id="app" :style="{'overflow-y': assessScroll()}">

    <router-view v-on:successfulLogin="initLogin($event)" v-on:expiredOrInvalid="forceLogout($event)" v-on:bldgDeleted="forceLogout()" v-on:updateNav="initNav($event)" v-on:resetBundle="reBundle($event)" v-on:setHeaderFromStorage="headerStateSet($event)"/>

    <header-component :headerData="headerData" :admin="admin" v-if="showHeader" v-on:showSettings="togSettings($event)" style="position:fixed;top:0;width:100vw;" v-on:headerLogout="forceLogout()"></header-component>

    <nav-component :navData="navData" v-if="showNav"></nav-component>

    <settings-component :toTeam="toTeam" :user="admin" :team="team" :tkn="tkn" :style="{'height': calcHeight()}" v-if="settings" v-on:settingsClose="settings = false" v-on:initLogout="forceLogout()"></settings-component>
    <!-- <loading-component style="width:100%;height:100%;" v-if="loading"></loading-component> -->
  </div>
</template>

<script>

import axios from 'axios';

import Nav from './components/util/Nav';
import Header from './components/util/Header';
import Settings from './components/util/Settings';
import Loading from './components/util/Loading';

import Bundle from './src/bundle';
import Rest from './src/rest';

export default {
  name: 'App',
  data(){
    return {
      loggedIn: false,
      showNav: false,
      showHeader: false,
      headerData: {},
      navData: {},
      org: {},
      buildings: [],
      admin: {},
      bundle: {},
      org: {},
      buildings: {},
      mobile: false,
      team: {},
      settings: false,
      overflow: 'scroll',
      devices: [],
      toTeam: false,
      tkn: '',
      loading: false
    }
  },
  components: {
    'nav-component': Nav,
    'header-component': Header,
    'settings-component': Settings
  },
  methods: {
    initLogin(event){
      this.loading = true;
      try {
        this.tkn = event;
        this.getBundle(event)
          .then(bundle => {
            this.initialSetup(bundle);
            this.loggedIn = true;
            this.initNav({org: this.bundle, type: 'org'});
            this.loading = false;
            this.$router.push({
              name: this.mobile ? 'Org' : 'WebOrg',
              params: {
                bundle: this.bundle,
                tkn: this.tkn
              }
            });
          }).catch(err => {
            console.log({ERROR: err});
            alert('There was an error retrieving your data. Please try logging in again and, if the problem persists, contact Breezi support');
            this.loggedIn = false;
          })
      } catch(err) {
        alert('There was an error retrieving your data. Please try logging in again and, if the problem persists, contact Breezi support');
        this.loggedIn = false;
      }
    },
    initialSetup(bundle){
      this.bundle = Bundle.formatBundle(bundle.data);
      this.admin = this.bundle.admin;
      this.org = this.bundle.organization;
    },
    getBundle(tkn){
      const url = 'https://hr1og7d86l.execute-api.eu-west-1.amazonaws.com/breezi_backend_application_service_gateway_matt/bundle';
      let headers = {
        headers: {
          Authorization: tkn,
          'Content-Type': 'application/json'
        }
      };
      return new Promise((resolve, reject) => {
        axios.get(url, headers)
          .then(bundle => {
            console.log({RAW_BUNDLE: bundle});
            Rest.getTeam(headers)
              .then(team => {
                this.team = team;
                resolve(bundle);
              })
          }).catch(err => reject(null));
      });
    },
    formatBundle(){
      return new Promise(resolve => {

      });
    },
    forceLogout(){
      let cookiesToDel = ['breeziProTkn', 'breeziNavStep', 'breeziBldg', 'breeziUnits'].forEach(c => {
        this.$cookies.remove(c);
      })
      this.showHeader = false;
      this.showNav = false;
      this.settings = false;
      this.$router.push({
        name: 'Splash'
      });
      this.loggedIn = false;
    },
    initNav(obj){
      let newNav = {
        team: {},
        org: {},
        bldg: {},
        floor: {},
        room: {},
        tkn: this.tkn
      }
      if (obj.type === 'org') {
        this.$cookies.remove('breeziNavStep');
        this.$cookies.set('breeziNavStep', 'org');
        if (obj.team) {
          this.team = obj.team;
          this.admin = obj.org.admin;
        }
        newNav.org = obj.org;
      }
      if (obj.type === 'bldg') {
        if (obj.bundle) {
          newNav.org = obj.bundle.org;
          this.team = obj.team;
          console.log(obj.bundle)
          this.admin = obj.bundle.admin;
        } else {
          newNav.org = this.navData.org;
        }
        newNav.bldg = obj.bldg;
      }
      if (obj.type === 'floor') {
        if (obj.bundle) {
          newNav.org = obj.bundle.org;
          newNav.bldg = obj.bldg;
          this.team = obj.team;
          this.admin = obj.bundle.admin;
        } else {
          newNav.org = this.navData.org;
          newNav.bldg = this.navData.bldg;
        }
        newNav.floor = obj.floor;
      }
      if (obj.type === 'room') {
        if (obj.bundle) {
          newNav.org = obj.bundle.org;
          newNav.bldg = obj.bldg;
          newNav.floor = obj.floor;
          this.team = obj.team;
          this.admin = obj.bundle.admin;
        } else {
          newNav.org = this.navData.org;
          newNav.bldg = this.navData.bldg;
          newNav.floor = this.navData.floor;
        }
        newNav.room = obj.room;
      }
      this.navData = newNav;
      this.showNav = true;
      this.updateHeader(obj);
    },
    updateHeader(data){
      let newHeader = {};
      newHeader.tkn = this.tkn;
      if (data.type === 'org' && data.org.organization) {
        newHeader.name = data.org.organization.name;
        newHeader.lgText = data.org.buildings.length;
        newHeader.subText = `Building${data.org.buildings.buildings && data.org.buildings.buildings.length === 1 ? '' : 's'} monitored`;
        newHeader.crit = data.org.buildings.overallAlerts ? data.org.buildings.overallAlerts.crit : 0;
        newHeader.bad = data.org.buildings.overallAlerts ? data.org.buildings.overallAlerts.bad : 0;
        newHeader.type = 'org';
      }
      if (data.type === 'org' && data.org.org) {
        newHeader.name = data.org.org.name;
        newHeader.lgText = data.org.buildings.buildings.length;
        newHeader.subText = `Building${data.org.buildings.buildings.length === 1 ? '' : 's'} monitored`;
        newHeader.crit = data.org.buildings.overallAlerts ? data.org.buildings.overallAlerts.crit : 0;
        newHeader.bad = data.org.buildings.overallAlerts ? data.org.buildings.overallAlerts.bad : 0;
        newHeader.type = 'org';
      }
      if (data.type === 'bldg') {
        let monitored = data.bldg.floors.filter(f => f.active).length;
        newHeader.name = data.bldg.name;
        newHeader.lgText = monitored;
        newHeader.subText = `of ${data.bldg.floors.length - 1} floor${data.bldg.floors.map(f => (f.active && f.active === true)).length === 1 ? '' : 's'} monitored`;
        newHeader.crit = data.bldg.overallAlerts.crit;
        newHeader.bad = data.bldg.overallAlerts.bad;
        newHeader.type = 'bldg';
      }
      if (data.type === 'floor') {
        newHeader.name = data.floor.name ? data.floor.name : `Floor ${data.floor.index}`;
        newHeader.lgText = data.floor.rooms.length;
        newHeader.subText = `Room${data.floor.rooms.length === 1 ? '' : 's'} monitored`;
        newHeader.crit = data.floor.overallAlerts.crit;
        newHeader.bad = data.floor.overallAlerts.bad;
        newHeader.type = 'floor';
      }
      if (data.type === 'room') {
        newHeader.name = data.room.hvacName;
        newHeader.lgText = '';
        newHeader.subText = '';
        newHeader.crit = data.room.overallAlerts.crit;
        newHeader.bad = data.room.overallAlerts.bad;
        newHeader.type = 'room';
      }
      this.headerData = newHeader;
      this.showHeader = true;
    },
    togSettings(event){
      this.overflow = event === true
        ? 'hidden'
        : 'scroll';
      this.settings = event;
    },
    calcHeight(){
      return `${window.innerHeight - 52}px`;
    },
    assessScroll(){
      return !this.mobile ? 'scroll' : 'hidden';
    },
    headerStateSet(event){
      try {
        let step = this.$cookies.get('breeziNavStep');
        this.getBundle(this.$cookies.get('breeziProTkn'))
          .then(bundle => {
            this.initialSetup(bundle);
            switch (step) {
              case 'org':
                this.initNav({type: 'org', org: this.bundle});
              break;
              case 'bldg':
                let bldg = this.bundle.buildings.buildings.find(b => b.id === this.$cookies.get('breeziBldg'));
                console.log(bldg);
              break;
            }
          });
      } catch(err) {
        alert(`There was an error you will be logged out. Please contact breezi support if the issue persists`);
        this.forceLogout();
      }
    }
  },
  mounted(){
    this.screenSize = window.innerWidth;
    this.mobile = this.screenSize <= 500
      ? true
      : false;
    this.assessScroll();
  }
}

function aggAlerts(obj){
  Object.keys(obj.alerts).reduce((a, i) => {
    if (obj.alerts[i].crit > 0) {
      a.crit += obj.alerts[i].crit;
    }
    if (obj.alerts[i].bad > 0) {
      a.bad += obj.alerts[i].crit;
    }
    return a;
  }, {crit: 0, bad: 0});
}

</script>

<style>
#app {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  width: 100vw;
  height: 100vh;
  background-color: #151920;
  font-family: 'Comfortaa', sans-serif;
  color: #dfe2ea;
  padding: 0;
  padding-bottom: 60px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-top: 140px;
}

a:visited {
  color: #e6e4ec;
}

a:hover {
  color: #e6e4ec;
}

a:active {
  color: #e6e4ec;
}

.onboard-form {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
}

.onboard-input {
  width: 300px;
  height: 30px;
  background-color: rgba(138, 138, 138, 0.3);
  margin-bottom: 5px;
  border: none;
  border-bottom: 1px solid #dfe2ea;
  padding-left: 4px;
  padding-right: 4px;
}

.back-btn {
  justify-content: center;
  height: 40px;
  background-color: rgba(203, 134, 82, 0.5);
  width: 30%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px;
  color: #dfe2ea;
  box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
  cursor: pointer;
  border-radius: 5px;
  border: 1px solid #cb8652;
}

.back-btn:hover {
  background: white;
  color: gray;
  text-decoration: none;
}

.splash-panel {
  width: 400px;
  max-width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 10px;
  padding-right: 10px;
}

  .splash-btn {
    width: 30%;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(53, 143, 240, 0.5);
    border: 1px solid #35b7f0;
    box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
    cursor: pointer;
    border-radius: 5px;
    padding-left: 5px;
    padding-right: 5px;
    color: #e7eee0;
  }

    .splash-btn:hover {
      background: #35b7f0;
      color: white;
      text-decoration: none;
    }

::-webkit-scrollbar {
  width: 1.5px;
}

::-webkit-scrollbar-track {
  background-color: rgba(0, 0, 0, 0);
}

::-webkit-scrollbar-thumb {
  background-color: rgba(0, 153, 191, 0.8);
}

.onboard-form {
  overflow-y: hidden;
  height: 100%;
  width: 100%;
  padding-bottom: 10%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
}

.onboard-header {
  width: 100%;
  height: 35%;
  border-bottom: 1px solid rgb(0, 153, 191);
  background-color: #262529;
}

.onboard-input-panel {
  height: 65%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
}

  .onboard-input-panel label {
    margin-bottom: 18px;
  }

  .onboard-multi-btn {
    width: 300px;
    height: 50px;
    background-color: rgba(255, 255, 255, 0.3);
    border: 1px solid white;
    color: white;
    text-decoration: none;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 10px;
  }

  .onboard-multi-btn:focus {
    color: white;
    text-decoration: none;
  }

  .onboard-multi-btn:hover {
    color: white;
    text-decoration: none;
  }


  .onboard-progress {
    width: 80%;
    height: 80%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
  }

  .prog-node {
    width: 25%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .prog-active p {
    color: rgb(0, 153, 191);
  }

  .prog-content {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
  }

  .prog-ball {
    margin: 8px;
    border-radius: 100%;
  }

  .prog-bar {
    background-color: gray;
    height: 4px;
    width: 50%;
    display: flex;
    display: none;
  }

  .prog-active .prog-ball {
    width: 50px;
    height: 50px;
    background-color: rgb(0, 153, 191);
    border-radius: 100%;
    box-shadow: 0 0 3px white;
  }

  .prog-active small {
    position: absolute;
    bottom: 0;
    color: rgb(0, 153, 191);
    font-size: 1em;
  }

  .prog-inactive .prog-ball {
    width: 30px;
    height: 30px;
    border: 3px dashed rgb(0, 153, 191);
    border-radius: 100%;
  }

  .prog-past .prog-bar {
    display: flex;
  }

  .prog-inactive small {
    position: absolute;
    bottom: 0;
    font-size: 1em;
  }

  .prog-past small {
    position: absolute;
    bottom: 0;
    font-size: 1em;
  }

  .prog-past .prog-content {
    cursor: pointer;
  }

  .prog-past small {
    color: gray;
  }

  .prog-past .prog-ball {
    background-color: gray;
    width: 30px;
    height: 30px;
  }

a {
  cursor: pointer;
}

.org-scene {
  width: 17%;
  height: 90%;
  position: relative;
  margin: 10px;
  perspective: 1000px;
}

.org-card {
  position: absolute;
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transform-origin: center;
  transition: 0.5s;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  border: 1px solid rgba(0, 153, 191);
  cursor: pointer;
}

.card__face {
  position: absolute;
  width: 100%;
  height: 100%;
  color: white;
  text-align: center;
  font-weight: bold;
  font-size: 40px;
  backface-visibility: hidden;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
}

.card__face img {
  height: 40%;
  width: auto;
}

.flip-img {
  height: 30%;
  width: auto;
  transform: rotateX(180deg);
  backface-visibility: hidden;
}

.org-is-flipped {
  background-color: rgb(0, 153, 191);
  transform: rotateX(180deg);
}

.face-front {

}

.face-back {
  transform: rotateX(-180deg);
  background-color: rgb(0, 153, 191);
  backface-visibility: visible;
}

.sub-panel {
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}

.sub-panel h6 {
  width: 35%;
}

.add-web {
  height: 99%;
  width: 100%;
  background-color: #262529;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  margin-right: 10px;
  border: 1px solid rgba(0, 153, 191);
}

.add-web:hover {
  box-shadow: 0 0 8px #e1f1f1;
}

.add-web h1 {
  height: 100%;
  width: 100%;
  color: #e1f1f1;
  font-size: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-bottom: 8px;
}

.add-web img {
  height: 80%;
  width: auto;
}

</style>
