import Vue from 'vue'
import Router from 'vue-router'
import Splash from '@/components/splash/Splash';
import Org from '@/components/org/Org';
import WebOrg from '@/components/org/BubbleOrg';
import Building from '@/components/building/Building';
import Floor from '@/components/floor/Floor';
import Room from '@/components/room/Room';
import WebRoom from '@/components/room/WebRoom';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Splash',
      component: Splash
    },
    {
      path: '/org',
      name: 'Org',
      component: Org
    },
    {
      path: '/webOrg',
      name: 'WebOrg',
      component: WebOrg
    },
    {
      path: '/building',
      name: 'Building',
      component: Building
    },
    {
      path: '/floor',
      name: 'Floor',
      component: Floor
    },
    {
      path: '/room',
      name: 'Room',
      component: Room
    },
    {
      path: 'room',
      name: 'WebRoom',
      component: WebRoom
    }
  ]
})
